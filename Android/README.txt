Vasama is an application that shows lightning strikes on a map. It parses
spark.clx compatible (spark.txd + mapstrip) formatted data.

This is the Android port. Should work on 4.0.3 and newer.

This is my first Android application ever, so the code probably looks quite
ugly.

Vasama is licensed under GPL v3. See LICENSE.txt.
