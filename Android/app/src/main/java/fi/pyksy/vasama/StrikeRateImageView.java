package fi.pyksy.vasama;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;

/**
 * Created by pyksy on 14.4.2015.
 */
public class StrikeRateImageView extends ImageView implements GestureDetector.OnGestureListener {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String   TAG = "StrikeRateImageView";
    private final GestureDetector gestureDetector;
    private final VasamaActivity  vasamaActivity;

// ================================================================================================
// Constructor
// ================================================================================================
    public StrikeRateImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(TAG, "StrikeRateImageView()");

        gestureDetector = new GestureDetector(context, this);
        vasamaActivity = (VasamaActivity) super.getContext();
    }

// ================================================================================================
// onTouchEvent
// ================================================================================================
    @Override
    public boolean onTouchEvent(MotionEvent e) {

        if(e.getAction() == MotionEvent.ACTION_UP ||
           e.getAction() == MotionEvent.ACTION_CANCEL) {
            vasamaActivity.setInhibitUpdate(false);
        }

        return gestureDetector.onTouchEvent(e) || super.onTouchEvent(e);
    }

// ================================================================================================
// onDown()
// ================================================================================================
    public boolean onDown(MotionEvent e) {
        return true;
    }

// ================================================================================================
// onShowPress()
// ================================================================================================
    public void onShowPress(MotionEvent e) {
        vasamaActivity.setInhibitUpdate(true);
    }

// ================================================================================================
// onSingleTapUp()
// ================================================================================================
    public boolean onSingleTapUp(MotionEvent e) {
        int x = (int) e.getX();
        if (x > 0 && x < getWidth()) {
            vasamaActivity.setInhibitUpdate(true);
            vasamaActivity.scrollRateGraph(x);
        }
        return true;
    }

// ================================================================================================
// onScroll()
// ================================================================================================
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        int x = (int) e2.getX();
        if (x > 0 && x < getWidth()) {
            vasamaActivity.setInhibitUpdate(true);
            vasamaActivity.scrollRateGraph(x);
        }

        return true;
    }

// ================================================================================================
// onLongPress()
// ================================================================================================
    public void onLongPress(MotionEvent e) {
        //
    }

// ================================================================================================
// onFling()
// ================================================================================================
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }
}
