package fi.pyksy.vasama;

import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by pyksy on 7.4.2015.
 */
public class StrikeDataDownloader extends AsyncTask<String, Void, Void> {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String    TAG = "StrikeDataDownloader";
    private final StrikeDataParser strikeDataParser;
    private final VasamaActivity   activity;
    private boolean                downloadSuccess;

// ================================================================================================
// Constructor
// ================================================================================================
    public StrikeDataDownloader(StrikeDataParser aStrikeDataHandler, VasamaActivity aActivity) {
        Log.d(TAG, "StrikeDataDownloader()");

        strikeDataParser = aStrikeDataHandler;
        activity = aActivity;   // todo use messageHandler
    }

// ================================================================================================
// doInBackground
// ================================================================================================
    protected Void doInBackground(String... aUrls) {
        Log.d(TAG, "doInBackground()");

        String mapUrl = aUrls[0];
        try {
            String line;
            InputStream inputStream = new URL(mapUrl).openStream();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "ISO-8859-1"));

            try {
                while ((line = bufferedReader.readLine()) != null) {
                    strikeDataParser.parseStrikeDataLine(line);
                }
                downloadSuccess = true;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            Log.e("doInBackground() Error ", e.getMessage());
            e.printStackTrace();
            downloadSuccess = false;
        }

        return null;
    }

// ================================================================================================
// onPostExecute()
// ================================================================================================
    protected void onPostExecute(Void aVoid) {
        Log.d(TAG, "onPostExecute()");

        activity.dataParsed(downloadSuccess);  // todo use messageHandler
    }
}
