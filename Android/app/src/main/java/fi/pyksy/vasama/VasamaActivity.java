package fi.pyksy.vasama;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class VasamaActivity extends ActionBarActivity {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String      TAG = "VasamaActivity";
    private static final int         INTENT_GET_SITE = 1;
    private static final String      PREFS_NAME = "VasamaPreferences";
    private static final String      BASEURL_KEY = "BaseUrlKey";
    private static final String      RANGE_KEY = "RangeKey";
    private String                   baseUrl = "http://pyksy.dy.fi/temp/";
    private String                   rateString;
    private String                   rangeString;
    private TextView                 locationTitleText;
    private EditText                 urlEditText;
    private TextView                 rateTextView;
    private TextView                 rangeTextView;
    private TextView                 dateTextView;
    private StrikeImageView          mapImageView;
    private StrikeImageView          strikeImageView;
    private StrikeRateImageView      strikeRateImageView;
    private StrikeImageGfxTask       strikeImageGfxTask;
    private MapImageParser           mapImageParser;
    private MapImageDownloader       mapImageDownloader;
    private StrikeDataParser         strikeDataParser;
    private StrikeDataDownloader     strikeDataDownloader;
    private ScheduledExecutorService strikeImageTaskExecutor;
    private Handler                  messageHandler;
    private SharedPreferences        sharedPreferences;

// ================================================================================================
// onCreate()
// ================================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate() Thread id " + android.os.Process.myTid());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vasama);

        // Create handler messageHandler to be called when StrikeImageRunnable has finished
        // creating a new strike bitmap
        messageHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                updateImageViews();
            }
        };

        sharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        readPreferences(BASEURL_KEY);

        // Init member variables
        locationTitleText = (TextView) findViewById(R.id.locationTitleText);
        urlEditText = (EditText) findViewById(R.id.urlEditText);
        rateTextView = (TextView) findViewById(R.id.rateTextView);
        rangeTextView = (TextView) findViewById(R.id.rangeTextView);
        dateTextView = (TextView) findViewById(R.id.dateTextView);
        mapImageView = (StrikeImageView) findViewById(R.id.mapImageView);
        strikeImageView = (StrikeImageView) findViewById(R.id.strikeImageView);
        strikeRateImageView = (StrikeRateImageView) findViewById(R.id.strikeRateImageView);
        mapImageParser = new MapImageParser(mapImageView);
        readPreferences(RANGE_KEY);
        mapImageDownloader = new MapImageDownloader(mapImageParser);
        mapImageDownloader.execute(baseUrl + "mapstrip.png");
        strikeDataParser = new StrikeDataParser();
        strikeImageGfxTask = new StrikeImageGfxTask(
                strikeDataParser.getStrikeData(),
                messageHandler);
        strikeDataDownloader = new StrikeDataDownloader(strikeDataParser, this);
        strikeDataDownloader.execute(baseUrl + "spark.txd");

        rateString = getString(R.string.rate);
        rangeString = getString(R.string.range);

        urlEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == urlEditText.getId())
                {
                    strikeImageGfxTask.setInhibitUpdate(true);
                    urlEditText.setCursorVisible(true);
                }
            }
        });

        urlEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                Log.d(TAG, "onEditorAction()");
                urlEditText.setCursorVisible(false);

                // todo fixme is this InputMethodManager copypaste really needed?
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(
                        urlEditText.getApplicationWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

                baseUrl = urlEditText.getText().toString();

                // add trailing slash if missing
                if (baseUrl.length() > 0 && !baseUrl.substring(baseUrl.length()-1).equals("/")) {
                    baseUrl += "/";
                }

                // add leading http:// if missing
                if (baseUrl.length() > 7 && !baseUrl.substring(7).equals("http://")) {
                    baseUrl = "http://" + baseUrl;
                }

                strikeImageGfxTask.setInhibitUpdate(false);
                redownload();

                return false;
            }
        });
    }

// ================================================================================================
// onPause()
// ================================================================================================
    @Override
    protected void onPause() {
        Log.d(TAG, "onPause()");

        super.onPause();

        strikeImageGfxTask.setInhibitUpdate(true);
    }

// ================================================================================================
// onResume()
// ================================================================================================
    @Override
    protected void onResume() {
        Log.d(TAG, "onResume()");

        super.onResume();

        strikeImageGfxTask.setInhibitUpdate(false);
    }

// ================================================================================================
// onDestroy()
// ================================================================================================
    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy()");

        super.onDestroy();

        // Shut down strike image generator thread
        strikeImageTaskExecutor.shutdownNow();
        try {
            Log.d(TAG, "onCreate() awaiting termination up to 10 seconds");
            strikeImageTaskExecutor.awaitTermination(10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

// ================================================================================================
// onCreateOptionsMenu()
// ================================================================================================
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vasama, menu);
        return true;
    }

// ================================================================================================
// onOptionsItemSelected()
// ================================================================================================
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected()");

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_location) {
            openLocationSelection();
            return true;
        } else if (id == R.id.action_about) {
            showAbout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

// ================================================================================================
// onBackPressed
// ================================================================================================
    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed()");

        super.onBackPressed();
    }

// ================================================================================================
// onLocationTitleTextClicked()
// ================================================================================================
    public void onLocationTitleTextClicked(View v) {
        Log.d(TAG, "onLocationTitleTextClicked()");

        openLocationSelection();
    }

// ================================================================================================
// onActivityResult()
// ================================================================================================
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == INTENT_GET_SITE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    Site site = (Site) bundle.getSerializable("NEW_SITE");
                    Log.d(TAG, "onActivityResult() got site: " + site.getLocation());

                    baseUrl = site.getUrl();
                    redownload();
                }
            } else {
                startStikeImageExecutor();
            }
        }
    }

// ================================================================================================
// dataParsed()
// ================================================================================================
    public void dataParsed(boolean aSuccess) {
        // strike data has been parsed
        Log.d(TAG, "dataParsed() success = " + aSuccess);

        if (aSuccess) {
            int currentRange = mapImageParser.getRange();

            locationTitleText.setText(strikeDataParser.getLocationData().getLocation());
            urlEditText.setText(baseUrl);
            writePreferences(BASEURL_KEY);

            strikeImageGfxTask.setStrikeData(strikeDataParser.getStrikeData());

            // todo: race condition? what if StrikeRateImageView has not been laid out yet?
            strikeImageGfxTask.setRateBitmapSizes(
                    strikeRateImageView.getWidth(),
                    strikeRateImageView.getHeight());
            strikeImageGfxTask.calculateRateRects();
            strikeImageGfxTask.setRange(currentRange);
            rangeTextView.setText(String.format(rangeString,
                    strikeDataParser.getStrikeData().getRange(currentRange)));

            startStikeImageExecutor();
        } else {
            Log.d(TAG, "dataParsed() download failed");
            locationTitleText.setText(getString(R.string.location_failed));
            strikeImageGfxTask.setInhibitUpdate(true);
            strikeImageGfxTask.clearStrikeBitmap();
            strikeImageView.invalidate();

            Toast.makeText(getApplicationContext(),
                    getString(R.string.location_failed),
                    Toast.LENGTH_LONG).show();

            // todo fixme range still scrollable
        }
    }

// ================================================================================================
// startStikeImageExecutor()
// ================================================================================================
    public void startStikeImageExecutor() {
        Log.d(TAG, "startStikeImageExecutor()");

        if (strikeImageTaskExecutor == null || strikeImageTaskExecutor.isShutdown()) {
            strikeImageTaskExecutor = Executors.newScheduledThreadPool(3);
            strikeImageTaskExecutor.scheduleAtFixedRate(
                    strikeImageGfxTask, 1, 1, TimeUnit.SECONDS);
        }
    }

// ================================================================================================
// openLocationSelection()
// ================================================================================================
    public void openLocationSelection() {
        Log.d(TAG, "openLocationSelection()");

        Intent intent = new Intent(this, VasamaSitelistActivity.class);
        startActivityForResult(intent, INTENT_GET_SITE);
    }

// ================================================================================================
// redownload()
// ================================================================================================
    public void redownload() {
        locationTitleText.setText(getString(R.string.location_loading));

        mapImageDownloader = null;
        mapImageDownloader = new MapImageDownloader(mapImageParser);
        mapImageDownloader.execute(baseUrl + "mapstrip.png");

        strikeDataParser = null;
        strikeDataParser = new StrikeDataParser();
        strikeDataDownloader = null;
        strikeDataDownloader = new StrikeDataDownloader(strikeDataParser, this);
        strikeDataDownloader.execute(baseUrl + "spark.txd");
    }

// ================================================================================================
// readPreferences()
// ================================================================================================
    public void readPreferences(String aKey) {
        switch (aKey) {
            case BASEURL_KEY:
                if (sharedPreferences.contains(BASEURL_KEY)) {
                    baseUrl = sharedPreferences.getString(BASEURL_KEY, "");
                    Log.d(TAG, "readPreferences() baseUrl = " + baseUrl);
                }
                break;
            case RANGE_KEY:
                if (sharedPreferences.contains(RANGE_KEY)) {
                    int range = sharedPreferences.getInt(RANGE_KEY, 0);
                    mapImageParser.setRange(range);
                    Log.d(TAG, "readPreferences() currentRange = " + range);
                }
                break;
        }
    }

// ================================================================================================
// writePreferences()
// ================================================================================================
    public void writePreferences(String aKey) {
        SharedPreferences.Editor editor = sharedPreferences.edit();

        switch (aKey) {
            case BASEURL_KEY:
                editor.putString(BASEURL_KEY, baseUrl);
                editor.apply();
                Log.d(TAG, "writePreferences() baseUrl = " + baseUrl);
                break;
            case RANGE_KEY:
                int range = mapImageParser.getRange();
                editor.putInt(RANGE_KEY, range);
                editor.apply();
                Log.d(TAG, "writePreferences() currentRange = " + range);
                break;
        }
    }

// ================================================================================================
// updateImageViews()
// ================================================================================================
    public void updateImageViews() {
        strikeImageView.setImageBitmap(strikeImageGfxTask.getStrikeBitmap());
        strikeRateImageView.setImageBitmap(strikeImageGfxTask.getRateBitmap());
        strikeImageView.invalidate();
        strikeRateImageView.invalidate();

        int imageIndex = strikeImageGfxTask.getImageIndex();
        rateTextView.setText(String.format(rateString,
                strikeDataParser.getStrikeData().getStrikeRate(
                        imageIndex)));
        dateTextView.setText(strikeDataParser.getLocationData().getDateString(imageIndex));
    }

// ================================================================================================
// nextRange()
// ================================================================================================
    public void nextRange() {
        Log.d(TAG, "nextRange()");

        int currentRange = mapImageParser.nextRange();

        mapImageParser.setImageViewMap(currentRange);
        mapImageView.invalidate();

        strikeImageGfxTask.setRange(currentRange);
        strikeImageGfxTask.clearStrikeBitmap();
        strikeImageView.setImageBitmap(strikeImageGfxTask.getStrikeBitmap());
        strikeImageView.invalidate();
        rangeTextView.setText(String.format(
                rangeString,
                strikeDataParser.getStrikeData().getRange(currentRange)));

        writePreferences(RANGE_KEY);
    }

// ================================================================================================
// scrollRateGraph()
// ================================================================================================
    public void scrollRateGraph(int aX) {
        strikeImageGfxTask.scrollRateGraph(aX);
        strikeImageGfxTask.drawStrikeBitmap();
        strikeImageGfxTask.drawRateBitmap();
        updateImageViews();
    }

// ================================================================================================
// setInhibitUpdate()
// ================================================================================================
    public void setInhibitUpdate(boolean aInhibitUpdate) {
        strikeImageGfxTask.setInhibitUpdate(aInhibitUpdate);
    }

// ================================================================================================
// showAbout()
// ================================================================================================
    protected void showAbout() {
        // Inflate the about message contents
        View messageView = getLayoutInflater().inflate(R.layout.about, null, false);

        // When linking text, force to always use default color. This works
        // around a pressed color state bug.
        TextView textView = (TextView) messageView.findViewById(R.id.about_credits);
        int defaultColor = textView.getTextColors().getDefaultColor();
        textView.setTextColor(defaultColor);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.app_name);
        builder.setView(messageView);
        builder.create();
        builder.show();
    }
}
