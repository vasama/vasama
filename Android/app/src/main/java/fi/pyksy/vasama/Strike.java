package fi.pyksy.vasama;

/**
 * Created by pyksy on 8.4.2015.
 */
public class Strike {
// ================================================================================================
// Variables
// ================================================================================================
    private final int     bearingDegrees;
    private final double  bearingRadians;
    private final int     distance;
    private final double  kmX;
    private final double  kmY;
    private final int     timeDelta;

// ================================================================================================
// Constructor
// ================================================================================================
    public Strike(int aTimeDelta, int aBearingDegrees, int aDistance) {
        timeDelta = aTimeDelta;
        bearingDegrees = aBearingDegrees;
        distance = aDistance;
        bearingRadians = Math.toRadians(bearingDegrees + 270);
        kmX = distance * Math.cos(bearingRadians);
        kmY = distance * Math.sin(bearingRadians);
    }

// ================================================================================================
// getBearingDegress()
// ================================================================================================
    public int getBearingDegrees() {
        return bearingDegrees;
    }

// ================================================================================================
// getBearingRadians
// ================================================================================================
    public double getBearingRadians() {
        return bearingRadians;
    }

// ================================================================================================
// getDistance()
// ================================================================================================
    public int getDistance() {
        return distance;
    }

// ================================================================================================
// getKmX()
// ================================================================================================
    public double getKmX() {
        return kmX;
    }

// ================================================================================================
// getKmY()
// ================================================================================================
    public double getKmY() {
        return kmY;
    }

// ================================================================================================
// getTimeDelta()
// ================================================================================================
    public int getTimeDelta() {
        return timeDelta;
    }
}
