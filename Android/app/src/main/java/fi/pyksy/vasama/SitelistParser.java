package fi.pyksy.vasama;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by pyksy on 19.4.2015.
 */
public class SitelistParser {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String   TAG = "SitelistParser";
    private final ArrayList<Site> sitelist;

// ================================================================================================
// Constructor
// ================================================================================================
    public SitelistParser() {
        Log.d(TAG, "SitelistParser()");

        sitelist = new ArrayList<Site>();
    }

// ================================================================================================
// parseSiteLine
// ================================================================================================
    public void parseSiteLine(String aLine) {
        Log.d(TAG, "parseSiteLine()");

        String[] lineParts = aLine.split("\\t");

        for (String column : lineParts) {
            Log.d(TAG, "column: " + column);
        }

        if (lineParts.length == 3) {
            Site site = new Site();
            site.setLocation(lineParts[0]);
            site.setDate(lineParts[1]);
            site.setUrl(lineParts[2]);
            sitelist.add(site);
        }
    }

// ================================================================================================
// getSitelist
// ================================================================================================
    public ArrayList<Site> getSitelist() {
        return sitelist;
    }

}
