package fi.pyksy.vasama;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by pyksy on 19.4.2015.
 */
public class SitelistAdapter extends BaseAdapter {
// ================================================================================================
// Variables
// ================================================================================================
    private       ArrayList<Site> listData;
    private final LayoutInflater  layoutInflater;

// ================================================================================================
// Constructor
// ================================================================================================
    public SitelistAdapter(Context context, ArrayList<Site> aListData) {
        listData = aListData;
        layoutInflater = LayoutInflater.from(context);
    }

// ================================================================================================
// getCount()
// ================================================================================================
    @Override
    public int getCount() {
        return listData.size();
    }

// ================================================================================================
// getItem()
// ================================================================================================
    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

// ================================================================================================
// getItemId()
// ================================================================================================
    @Override
    public long getItemId(int position) {
        return position;
    }

// ================================================================================================
// getView()
// ================================================================================================
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_locationlistview_layout, null);
            holder = new ViewHolder();
            holder.locationTextView = (TextView) convertView.findViewById(R.id.locationTextView);
            holder.dateTextView = (TextView) convertView.findViewById(R.id.dateTextView);
            holder.timezoneTextView = (TextView) convertView.findViewById(R.id.timezoneTextView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Site site = listData.get(position);
        holder.locationTextView.setText(site.getLocation());
        holder.dateTextView.setText(site.getDate());
        holder.timezoneTextView.setText(site.getTimezone());

        return convertView;
    }

// ================================================================================================
// setListData()
// ================================================================================================
    public void setListData(ArrayList<Site> aListData) {
        listData = aListData;
    }

// ================================================================================================
// ViewHolder
// ================================================================================================
    static class ViewHolder {
        TextView locationTextView;
        TextView dateTextView;
        TextView timezoneTextView;
    }
}