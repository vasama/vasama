package fi.pyksy.vasama;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by pyksy on 8.4.2015.
 */
public class StrikeImageGfxTask implements Runnable {
// ================================================================================================
// variables
// ================================================================================================
    private static final String TAG = "StrikeImageGfxTask";
    private static final int    strikeBitmapWidth = 360; // todo fixme stupid hardcoded width
    private int                 rateBitmapWidth = 0;
    private int                 rateBitmapHeight = 0;
    private int                 imageIndex = 0;
    private int                 rangeKm = 1000;
    private final int           rangePixels = strikeBitmapWidth / 2;
    private float               rateHeightRatio = 0;
    private float               rateColorRatio = 0;
    private float               rateBarWidth = 0;
    private boolean             inhibitUpdate = false;
    private StrikeData          strikeData;
    private final Handler       messageHandler;
    private Bitmap              strikeBitmap;
    private final Paint         strikePaint;
    private final Canvas        strikeCanvas;
    private Bitmap              rateBitmap;
    private final Paint         ratePaint;
    private ArrayList<Rect>     rateRectList;
    private int                 rateColorList[];
    private static final int    strikeColors[] = {
            0xFF660000,
            0xFF880000,
            0xFF990000,
            0xFFAA0000,
            0xFFAA2200,
            0xFFAA4400,
            0xFFAA6600,
            0xFFEE7700,
            0xFFFF9900,
            0xFFFFCC00,
            0xFFFFFF00,
            0xFFFFFFFF
    };

// ================================================================================================
// Constructor
// ================================================================================================
    public StrikeImageGfxTask(StrikeData aStrikeData, Handler aHandler) {
        Log.d(TAG, "StrikeImageGfxTask()");

        strikeData = aStrikeData;
        messageHandler = aHandler;

        strikePaint = new Paint();
        strikePaint.setStyle(Paint.Style.STROKE);
        strikePaint.setStrokeWidth(2);

        ratePaint = new Paint();
        ratePaint.setDither(false);
        ratePaint.setStyle(Paint.Style.FILL);

        strikeBitmap = Bitmap.createBitmap(
                strikeBitmapWidth,
                strikeBitmapWidth,
                Bitmap.Config.ARGB_8888);
        strikeCanvas = new Canvas(strikeBitmap);

    }

// ================================================================================================
// run()
// ================================================================================================
    @Override
    public void run() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
        Log.d(TAG, "run() inhibitupdate = " + inhibitUpdate);

        if (!inhibitUpdate) {
            imageIndex++;
            imageIndex %= 24;

            drawStrikeBitmap();
            drawRateBitmap();

            messageHandler.sendEmptyMessage(42);
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Log.d(TAG, "run() Thread.sleep() interrupted");
        }
    }

// ================================================================================================
// getImageIndex()
// ================================================================================================
    public int getImageIndex() {
        return imageIndex;
    }

// ================================================================================================
// setStrikeData()
// ================================================================================================
    public void setStrikeData(StrikeData aStrikeData) {
        strikeData = aStrikeData;
    }

// ================================================================================================
// drawStrikeBitmap()
// ================================================================================================
    public void drawStrikeBitmap() {
        Log.d(TAG, "drawStrikeBitmap()");
        strikeBitmap.eraseColor(android.graphics.Color.TRANSPARENT);

        int x;
        int y;
        int startImage = 0;
        int colorindex = 0;
        if (imageIndex > 11) {
            // plotting 12 iterations from current-11 to current.
            startImage = imageIndex - 11;
        } else {
            // plotting framenum iterations, starting with color 11-framenum
            colorindex = 11 - imageIndex;
        }

        // Loop through strike data
        for (int frameidx = startImage; frameidx <= imageIndex && colorindex < 12; frameidx++) {
            ArrayList<Strike> strikes = strikeData.getStrikes(frameidx);

            // loop through the strikes for this strike set
            for (Strike strike : strikes) {
                x = rangePixels + rangePixels * (int) strike.getKmX() / rangeKm;
                y = rangePixels + rangePixels * (int) strike.getKmY() / rangeKm;

                // Plot the strike
                strikePaint.setColor(strikeColors[colorindex]);
                strikeCanvas.drawLine(x, y - 6, x - 3, y, strikePaint);
                strikeCanvas.drawLine(x - 3, y, x + 3, y, strikePaint);
                strikeCanvas.drawLine(x + 3, y, x, y + 6, strikePaint);
            }

            // Increase color intensity for next round of strikes
            colorindex++;
        }

        strikePaint.setColor(Color.GREEN);
        strikeCanvas.drawLine(
                rangePixels, rangePixels - 5, rangePixels, rangePixels + 5, strikePaint);
        strikeCanvas.drawLine(
                rangePixels - 5, rangePixels, rangePixels + 5, rangePixels, strikePaint);
    }

// ================================================================================================
// drawRateBitmap()
// ================================================================================================
    public void drawRateBitmap() {
        Log.d(TAG, "drawRateBitmap()");
        // todo maybe just clear instead of recreating?
        rateBitmap = null;
        rateBitmap = Bitmap.createBitmap(
                rateBitmapWidth,
                rateBitmapHeight,
                Bitmap.Config.ARGB_8888);
        Canvas rateCanvas = new Canvas(rateBitmap);

        // Loop from 0 to requested frame num
        for (int stepnum = 0; stepnum <= imageIndex; stepnum++) {
            ratePaint.setColor(rateColorList[stepnum]);
            rateCanvas.drawRect(rateRectList.get(stepnum), ratePaint);
        }
    }

// ================================================================================================
// getStrikeBitmap()
// ================================================================================================
    public Bitmap getStrikeBitmap() {
        return strikeBitmap;
    }

// ================================================================================================
// setRange()
// ================================================================================================
    public void setRange(int aRangeIndex) {
        Log.d(TAG, "getStrikeBitmap() " + aRangeIndex);

        if (aRangeIndex < strikeData.getRanges().size()) {
            rangeKm = strikeData.getRange(aRangeIndex) / 2;
            if (rangeKm == 0) {
                rangeKm = 1000;
            }
        }
    }

// ================================================================================================
// clearStrikeBitmap()
// ================================================================================================
    public void clearStrikeBitmap() {
        Log.d(TAG, "clearStrikeBitmap()");

        strikeBitmap.eraseColor(android.graphics.Color.TRANSPARENT);

        strikePaint.setColor(Color.GREEN);
        strikeCanvas.drawLine(rangePixels, rangePixels - 5, rangePixels, rangePixels + 5, strikePaint);
        strikeCanvas.drawLine(rangePixels - 5, rangePixels, rangePixels + 5, rangePixels, strikePaint);
    }

// ================================================================================================
// calculateRateRects()
// ================================================================================================
    public void calculateRateRects() {
        Log.d(TAG, "calculateRateRects()");

        // Precalc a list of 24 strike rate rectangles to plot on the screen

        rateRectList = null;
        rateRectList = new ArrayList<>();
        rateColorList = null;
        rateColorList = new int[24];

        int colorindex;
        int color;
        float barHeight;
        float rateBarX;
        float rateBarY;
        int maxColor = strikeColors.length-1;

        // Loop from 0 to requested frame num
        for( int stepnum = 0; stepnum<24; stepnum++)
        {
            barHeight = strikeData.getStrikeRate(stepnum) * rateHeightRatio;
            rateBarX = (int) (stepnum * rateBarWidth);
            rateBarY = rateBitmapHeight - barHeight;
            Rect rect = new Rect(
                    (int) (rateBarX + 2),
                    (int) (rateBarY),
                    (int) (rateBarX + rateBarWidth - 2),
                    rateBitmapHeight);
            rateRectList.add(rect);

            colorindex = (int) ((double) strikeData.getStrikeRate(stepnum) /
                    (double) strikeData.getMaxStrikeRate() * maxColor);
            color = strikeColors[colorindex];
            rateColorList[stepnum] = color;
        }
    }

// ================================================================================================
// setRateBitmapSizes()
// ================================================================================================
    public void setRateBitmapSizes(int aWidth, int aHeight) {
        Log.d(TAG, "setStrikeBitmapSize() " + aWidth + ", " + aHeight);

        rateBitmapWidth = aWidth;
        rateBitmapHeight = aHeight;

        rateBarWidth = (float) aWidth / 24;
        rateHeightRatio = (float) aHeight / strikeData.getMaxStrikeRate();
        rateColorRatio = (float) (strikeColors.length-1) / aHeight;
    }

// ================================================================================================
// getRateBitmap()
// ================================================================================================
    public Bitmap getRateBitmap() {
        return rateBitmap;
    }

// ================================================================================================
// clearRateBitmap()
// ================================================================================================
    public void clearRateBitmap() {
        Log.d(TAG, "clearStrikeBitmap()");

        rateBitmap.eraseColor(android.graphics.Color.TRANSPARENT);
    }

// ================================================================================================
// scrollRateGraph()
// ================================================================================================
    public void scrollRateGraph(int aX) {
        imageIndex = (int) (aX / rateBarWidth);
    }

// ================================================================================================
// setInhibitUpdate()
// ================================================================================================
    public void setInhibitUpdate(boolean aInhibitUpdate) {
        inhibitUpdate = aInhibitUpdate;
    }

}
