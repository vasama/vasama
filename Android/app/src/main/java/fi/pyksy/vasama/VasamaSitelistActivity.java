package fi.pyksy.vasama;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;


public class VasamaSitelistActivity extends ActionBarActivity {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String TAG = "VasamaSitelistActivity";
    private SitelistParser      sitelistParser;
    private SitelistAdapter     sitelistAdapter;

// ================================================================================================
// onCreate
// ================================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vasama_sitelist);

        sitelistParser = new SitelistParser();
        SitelistDownloader sitelistDownloader;
        sitelistDownloader = new SitelistDownloader(sitelistParser, this);
        sitelistDownloader.execute("http://vasama.pyksy.fi/spark/spark_sitelist.csv");
        sitelistAdapter = new SitelistAdapter(this, new ArrayList<Site>());

        final ListView listView = (ListView) findViewById(R.id.sitelistView);
        listView.setAdapter(sitelistAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> a, View v, int position, long id) {
                Site site = (Site) listView.getItemAtPosition(position);
                Intent intent = new Intent();
                intent.putExtra("NEW_SITE", site);
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

// ================================================================================================
// onCreateOptionsMenu
// ================================================================================================
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_vasama_location, menu);
        return true;
    }

// ================================================================================================
// onOptionsItemSelected
// ================================================================================================
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected()");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_back) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

// ================================================================================================
// onBackPressed
// ================================================================================================
    @Override
    public void onBackPressed() {
        Log.d(TAG, "onBackPressed()");

        finish();
    }

// ================================================================================================
// sitelistParsed
// ================================================================================================
    public void sitelistParsed() {
        Log.d(TAG, "sitelistParsed() list size " + sitelistParser.getSitelist().size());

        sitelistAdapter.setListData(sitelistParser.getSitelist());
        sitelistAdapter.notifyDataSetChanged();
    }

}
