package fi.pyksy.vasama;

import android.util.Log;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by pyksy on 7.4.2015.
 */
public class LocationData {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String     TAG = "LocationData";
    private final GregorianCalendar calendar;
    private String                  location;
    private String                  timeZone;
    private final ArrayList<String> dateList;

// ================================================================================================
// Constructor
// ================================================================================================
    public LocationData() {
        Log.d(TAG, "LocationData()");

        calendar = new GregorianCalendar();
        clearDate();
        dateList = new ArrayList<String>();
    }

// ================================================================================================
// clearDate();
// ================================================================================================
    public void clearDate() {
        calendar.clear();
    }

// ================================================================================================
// parseDate()
// ================================================================================================
    public void parseDate(String aDate) {
        /**
         * Date format in spark.txd:
         * DAT+0A0512*
         *     YYMMDD for year, month, day
         */
        Log.d(TAG, "parseDate()");

        if (aDate.startsWith("DAT+")) {
            int year = 2000 + Integer.parseInt(aDate.substring(4, 6), 16);
            int month = Integer.parseInt(aDate.substring(6, 8), 16);
            int day = Integer.parseInt(aDate.substring(8, 10), 16);

            calendar.set(year, month, day);
        }

        Log.d(TAG, "parseDate() calendar is now " + calendar.getTime().toString());
    }

// ================================================================================================
// parseTime()
// ================================================================================================
    public void parseTime(String aTime) {
        /**
         * Time format in spark.txd:
         * TIM+1637*
         *     HHMM for hours and minutes elapsed since midnight
         */
        Log.d(TAG, "parseTime()");

        if (aTime.startsWith("TIM+")) {
            int hour = Integer.parseInt(aTime.substring(4, 6), 16);
            int minute = Integer.parseInt(aTime.substring(6, 8), 16);

            calendar.add(GregorianCalendar.HOUR, hour);
            calendar.add(GregorianCalendar.MINUTE, minute);
        }

        Log.d(TAG, "parseTime() calendar is now " + calendar.getTime().toString());
    }

// ================================================================================================
// getDate()
// ================================================================================================
    public Date getDate() {
        return calendar.getTime();
    }

// ================================================================================================
// parseLocation()
// ================================================================================================
    public void parseLocation(String aLocation) {
        /**
         * Location format in spark.txd:
         * LOC+Hameenlinna*
         *     LLLLLLLLLLL for location string in plain text
         */
        Log.d(TAG, "parseLocation()");

        if (aLocation.startsWith("LOC+")) {
            location = aLocation.substring(4, aLocation.length()-1);
        }

        Log.d(TAG, "parseLocation() set location " + location);
    }

// ================================================================================================
// getLocation()
// ================================================================================================
    public String getLocation() {
        Log.d(TAG, "getLocation() location " + location);

        return location;
    }

// ================================================================================================
// parseTimeZone()
// ================================================================================================
    public void parseTimeZone(String aTZ) {
        /**
         * Time zone format in spark.txd:
         * TIL+EET*
         *     TTT Time zone
         */
        Log.d(TAG, "parseTimeZone()");

        if (aTZ.startsWith("TIL+")) {
            timeZone = aTZ.substring(4, aTZ.length()-1);
            Log.d(TAG, "parseTimeZone() setting TZ " + timeZone);
        }

        generateDateStrings();
    }

// ================================================================================================
// getTimeZone()
// ================================================================================================
    public String getTimeZone() {
        Log.d(TAG, "getTimezone() tz " + timeZone);

        return timeZone;
    }

// ================================================================================================
// generateDateStrings()
// ================================================================================================
    public void generateDateStrings() {
        Log.d(TAG, "generateDateStrings()");

        dateList.clear();
        GregorianCalendar newCalendar = (GregorianCalendar) calendar.clone();
        newCalendar.add(GregorianCalendar.MINUTE, -115); // start from 1h55min back
        DateFormat dateFormat = DateFormat.getDateTimeInstance();

        for (int i=0; i<24; i++) {
            String dateString = dateFormat.format(newCalendar.getTime());
            newCalendar.add(GregorianCalendar.MINUTE, 5);
            dateList.add(dateString);
        }
    }

// ================================================================================================
// getDateString()
// ================================================================================================
    public String getDateString(int aIndex) {
        String dateString = null;

        if (aIndex < dateList.size()) {
            dateString = dateList.get(aIndex);
        }

        return dateString;
    }
}
