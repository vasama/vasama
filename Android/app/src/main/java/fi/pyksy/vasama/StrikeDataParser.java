package fi.pyksy.vasama;

import android.util.Log;

/**
 * Created by pyksy on 7.4.2015.
 */
public class StrikeDataParser {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String TAG = "StrikeDataParser";
    private final LocationData  locationData;
    private final StrikeData    strikeData;

// ================================================================================================
// Constructor
// ================================================================================================
    public StrikeDataParser() {
        Log.d(TAG, "StrikeDataParser()");

        locationData = new LocationData();
        strikeData = new StrikeData();
    }

// ================================================================================================
// getLocationData()
// ================================================================================================
    public LocationData getLocationData() {
        return locationData;
    }

// ================================================================================================
// getStrikeData()
// ================================================================================================
    public StrikeData getStrikeData() {
        return strikeData;
    }

// ================================================================================================
// parseStrikeData()
// ================================================================================================
    public void parseStrikeDataLine(String aLine) {
        Log.d(TAG, "parseStrikeDataLine()");

        String identifier = aLine.substring(0, 3);
        switch (identifier) {
            case "DAT":
                locationData.parseDate(aLine);
                break;

            case "TIM":
                locationData.parseTime(aLine);
                break;

            case "SRT":
                strikeData.parseStrikeRates(aLine);
                break;

            case "RNG":
                strikeData.parseRanges(aLine);
                break;

            case "LOC":
                locationData.parseLocation(aLine);
                break;

            case "TIL":
                locationData.parseTimeZone(aLine);
                break;

            default:
                if (aLine.startsWith("D")) {
                    strikeData.parseStrikes(aLine);
                }
                break;
        }
    }
}
