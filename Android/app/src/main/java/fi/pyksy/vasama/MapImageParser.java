package fi.pyksy.vasama;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by pyksy on 7.4.2015.
 */
public class MapImageParser {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String TAG = "MapImageParser";
    private static final int    bitmapCount = 3;
    private int                 currentRange = 1;
    private ArrayList<Bitmap>   bitmapArrayList = null;
    private final ImageView     mapImageView;

    public MapImageParser(ImageView aMapImageView) {
        mapImageView = aMapImageView;
    }

// ================================================================================================
// processMapstrip
// ================================================================================================
    public void processMapstrip(Bitmap aMapStrip) {
        Log.d(TAG, "processMapstrip() Thread id " + android.os.Process.myTid());

        if (bitmapArrayList == null) {
            bitmapArrayList = new ArrayList<Bitmap>();
        }
        bitmapArrayList.clear();

        if (aMapStrip != null) {
            Log.d(TAG, "processMapstrip() processing");
            for (int i=0; i<bitmapCount; i++) {
                Bitmap croppedBitmap = Bitmap.createBitmap(
                        aMapStrip,
                        0,
                        i * aMapStrip.getWidth(),
                        aMapStrip.getWidth(),
                        aMapStrip.getWidth());
                bitmapArrayList.add(croppedBitmap);
            }
        } else {
            for (int i=0; i<3; i++) {
                Bitmap bitmap = Bitmap.createBitmap(
                        1,
                        1,
                        Bitmap.Config.ARGB_8888);
                bitmap.eraseColor(android.graphics.Color.TRANSPARENT);
                bitmapArrayList.add(bitmap);
            }
        }
    }

// ================================================================================================
// getMap()
// ================================================================================================
    public Bitmap getMap(int aIndex) {
        if (aIndex < bitmapCount && bitmapArrayList != null && aIndex < bitmapArrayList.size()) {
            return bitmapArrayList.get(aIndex);
        } else {
            return null;
        }
    }

// ================================================================================================
// setImageViewMap
// ================================================================================================
    public void setImageViewMap(int aIndex) {
        mapImageView.setImageBitmap(getMap(aIndex));
    }

// ================================================================================================
// nextRange
// ================================================================================================
    public int nextRange() {
        Log.d(TAG, "nextRange()");

        currentRange++;
        currentRange %= 3;
        return currentRange;
    }

// ================================================================================================
// setRange
// ================================================================================================
    public void setRange(int aRange) {
        Log.d(TAG, "setRange() = " + aRange);
        currentRange = aRange;
    }

// ================================================================================================
// getRange
// ================================================================================================
    public int getRange() {
        Log.d(TAG, "getCurrentRange() = " + currentRange);
        return currentRange;
    }
}