package fi.pyksy.vasama;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by pyksy on 7.4.2015.
 */
public class StrikeData {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String                TAG = "StrikeData";
    private int                                maxStrikeRate = 1;
    private final ArrayList<Integer>           rangeList;
    private final ArrayList<Integer>           strikeRateList;
    private final ArrayList<ArrayList<Strike>> strikeList;

// ================================================================================================
// Constructor
// ================================================================================================
    public StrikeData() {
        Log.d(TAG, "StrikeData()");

        rangeList = new ArrayList<Integer>();
        strikeRateList = new ArrayList<Integer>();
        strikeList = new ArrayList<ArrayList<Strike>>();
    }

// ================================================================================================
// parseRanges()
// ================================================================================================
    public void parseRanges(String aRanges) {
        /**
         * Map range format in spark.txd:
         * RNG+3E81F412C0*
         *     AAABBBCCC for 3 map ranges in 12bit hexadecimal
         */
        Log.d(TAG, "parseRanges()");

        if (aRanges.startsWith("RNG+")) {
            Integer range;

            range = Integer.parseInt(aRanges.substring(4, 7), 16);
            rangeList.add(range);
            Log.d(TAG, "parseRanges() add range " + range);
            range = Integer.parseInt(aRanges.substring(7, 10), 16);
            rangeList.add(range);
            Log.d(TAG, "parseRanges() add range " + range);
            range = Integer.parseInt(aRanges.substring(10, 13), 16);
            rangeList.add(range);
            Log.d(TAG, "parseRanges() add range " + range);
        }
    }

// ================================================================================================
// getRanges()
// ================================================================================================
    public ArrayList<Integer> getRanges() {
        Log.d(TAG, "getRanges()");

        return rangeList;
    }

// ================================================================================================
// getRange()
// ================================================================================================
    public int getRange(int aIndex) {
        Log.d(TAG, "getRange()");

        int range = 0;

        if (aIndex < rangeList.size()) {
            range = rangeList.get(aIndex);
        }

        return range;
    }

// ================================================================================================
// parseStrikeRates()
// ================================================================================================
    public void parseStrikeRates(String aStrikeRates) {
        /**
         * Strike rate format in spark.txd:
         * SRT+003004005001004003002002001001001000001003003003002002000001 [ ... ]
         *     AAABBBCCCDDDEEE [ ... ] for strike rate in 12bit hexadecimal,
         *                             total 24 values
         */
        Log.d(TAG, "parseStrikeRates()");

        strikeRateList.clear();

        if (aStrikeRates.startsWith("SRT+") && aStrikeRates.length() > 76) {
            String rateStr;
            int strStart;

            for (int i=0; i<24; i++) {
                strStart = 3*i + 4;
                rateStr = aStrikeRates.substring(strStart, strStart+3);
                Integer rate = Integer.parseInt(rateStr, 16);
                strikeRateList.add(rate);
                if (rate > maxStrikeRate) {
                    maxStrikeRate = rate;
                }
            }
        }
    }

// ================================================================================================
// getStrikeRates()
// ================================================================================================
    public ArrayList<Integer> getStrikeRates() {
        Log.d(TAG, "getStrikeRates()");

        return strikeRateList;
    }

// ================================================================================================
// getStrikeRate()
// ================================================================================================
    public int getStrikeRate(int aIndex) {
        int rate = 0;

        if (aIndex < strikeRateList.size()) {
            rate = strikeRateList.get(aIndex);
        }

        return rate;
    }

// ================================================================================================
// parseStrikes()
// ================================================================================================
    public void parseStrikes(String aStrikes) {
        /**
         * Strike data format in spark.txd:
         * D05+23014419725223120A1982822330B3000000000000000000000000000000*
         *  TT BBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDD
         *  TT = Time elapsed in minutes in 8bit hexadecimal
         * BBB = Bearing in degrees (0-359) in decimal(!)
         * DDD = Distance in kilometres in 12bit hexadecimal
         */
        Log.d(TAG, "parseStrikeLocations()");

        if (aStrikes.startsWith("D") && aStrikes.length() > 64) {
            int locationStart;
            String timeDeltaStr;
            int timeDelta;
            String bearingDegreesStr;
            int bearingDegrees;
            String distanceStr;
            int distance;

            // Get time delta
            timeDeltaStr = aStrikes.substring(1, 3);
            timeDelta = Integer.parseInt(timeDeltaStr, 16);
            ArrayList<Strike> strikeRow = new ArrayList<Strike>();

            for (int i=0; i<10; i++) {
                // get bearing and distance
                locationStart = 6*i + 4;
                bearingDegreesStr = aStrikes.substring(locationStart, locationStart+3);
                bearingDegrees = Integer.parseInt(bearingDegreesStr);
                distanceStr = aStrikes.substring(locationStart+3, locationStart+6);
                distance = Integer.parseInt(distanceStr, 16);

                // add new Strike to list
                Strike strike = new Strike(timeDelta, bearingDegrees, distance);
                strikeRow.add(strike);
            }

            strikeList.add(strikeRow);
        }
    }

// ================================================================================================
// getStrikes()
// ================================================================================================
    public ArrayList<ArrayList<Strike>> getStrikes() {
        return strikeList;
    }

// ================================================================================================
// getStrikes()
// ================================================================================================
    public ArrayList<Strike> getStrikes(int aRow) {
        if (aRow < strikeList.size()) {
            return strikeList.get(aRow);
        }

        return null;
    }

// ================================================================================================
// getStrike()
// ================================================================================================
    public Strike getStrike(int aRow, int aIndex) {
        Strike strike = null;

        if ( aRow < strikeList.size() &&
                aIndex < strikeList.get(aRow).size()) {
            strike = strikeList.get(aRow).get(aIndex);
        }

        return strike;
    }

// ================================================================================================
// getMaxStrikeRate()
// ================================================================================================
    public int getMaxStrikeRate() {
        return maxStrikeRate;
    }
}
