package fi.pyksy.vasama;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageView;

/**
 * Created by pyksy on 8.4.2015.
 */
public class StrikeImageView extends ImageView implements GestureDetector.OnGestureListener {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String   TAG = "StrikeImageView";
    private final GestureDetector gestureDetector;

// ================================================================================================
// Constructor
// ================================================================================================
    public StrikeImageView(Context context) {
        super(context);
        Log.d(TAG, "StrikeImageView()");

        gestureDetector = new GestureDetector( context, this ) ;
    }
// ================================================================================================
// Constructor
// ================================================================================================
    public StrikeImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Log.d(TAG, "StrikeImageView()");

        gestureDetector = new GestureDetector( context, this ) ;
    }

// ================================================================================================
// onMeasure
// ================================================================================================
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // return width as height to set square size
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

// ================================================================================================
// onTouchEvent
// ================================================================================================
    @Override
    public boolean onTouchEvent(MotionEvent e)
    {
        Log.d(TAG, "onTouchEvent() " + e.toString());

        return gestureDetector.onTouchEvent(e) || super.onTouchEvent(e);
    }

// ================================================================================================
// onDown()
// ================================================================================================
    public boolean onDown(MotionEvent e) {
        return true;
    }

// ================================================================================================
// onShowPress()
// ================================================================================================
    public void onShowPress(MotionEvent e) {
        //
    }

// ================================================================================================
// onSingleTapUp()
// ================================================================================================
    public boolean onSingleTapUp(MotionEvent e) {
        Log.d(TAG, "onSingleTapUp()");

        VasamaActivity vasamaActivity = (VasamaActivity) super.getContext();
        vasamaActivity.nextRange();

        return true;
    }

// ================================================================================================
// onScroll()
// ================================================================================================
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

// ================================================================================================
// onLongPress()
// ================================================================================================
    public void onLongPress(MotionEvent e) {
        //
    }

// ================================================================================================
// onFling()
// ================================================================================================
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }
}
