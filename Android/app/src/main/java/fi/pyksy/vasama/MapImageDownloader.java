package fi.pyksy.vasama;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by pyksy on 7.4.2015.
 */
public class MapImageDownloader extends AsyncTask<String, Void, Bitmap> {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String  TAG = "MapImageDownloader";
    private final MapImageParser mapImageParser;

// ================================================================================================
// Constructor
// ================================================================================================
    public MapImageDownloader(MapImageParser aMapImageParser) {
        Log.d(TAG, "MapImageDownloader()");

        mapImageParser = aMapImageParser;
    }

// ================================================================================================
// doInBackground()
// ================================================================================================
    protected Bitmap doInBackground(String... aUrls) {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        Log.d(TAG, "doInBackground() Thread id " + android.os.Process.myTid());

        String mapUrl = aUrls[0];
        Bitmap dlImage = null;
        try {
            InputStream in = new URL(mapUrl).openStream();
            dlImage = BitmapFactory.decodeStream(in);
            mapImageParser.processMapstrip(dlImage);
        }
        catch (Exception e) {
            Log.e("doInBackground() error ", e.getMessage());
            e.printStackTrace();

            dlImage = Bitmap.createBitmap(
                    1,
                    3,
                    Bitmap.Config.ARGB_8888);
            mapImageParser.processMapstrip(dlImage);
        }

        return dlImage;
    }

// ================================================================================================
// onPostExecute()
// ================================================================================================
    protected void onPostExecute(Bitmap aBitmap) {
        Log.d(TAG, "onPostExecute() Thread id " + android.os.Process.myTid());

        mapImageParser.setImageViewMap(mapImageParser.getRange());
    }
}
