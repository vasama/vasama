package fi.pyksy.vasama;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by pyksy on 19.4.2015.
 */
public class SitelistDownloader extends AsyncTask<String, Void, Void> {
// ================================================================================================
// Variables
// ================================================================================================
    private static final String          TAG = "SiteListDownloader";
    private final SitelistParser         sitelistParser;
    private final VasamaSitelistActivity activity;

// ================================================================================================
// Constructor
// ================================================================================================
    public SitelistDownloader(SitelistParser aSitelistParser, VasamaSitelistActivity aActivity) {
        Log.d(TAG, "StrikeDataDownloader()");

        sitelistParser = aSitelistParser;
        activity = aActivity;   // todo use messageHandler
    }

// ================================================================================================
// doInBackground
// ================================================================================================
    protected Void doInBackground(String... aUrls) {
        Log.d(TAG, "doInBackground()");

        String mapUrl = aUrls[0];
        try {
            String line;
            InputStream inputStream = new URL(mapUrl).openStream();
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(inputStream, "ISO-8859-1"));

            try {
                while ((line = bufferedReader.readLine()) != null) {
                    sitelistParser.parseSiteLine(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        catch (Exception e) {
            Log.e("doInBackground() Error ", e.getMessage());
            e.printStackTrace();
            // todo show toast or something
        }

        return null;
    }

// ================================================================================================
// onPostExecute
// ================================================================================================
    @Override
    protected void onPostExecute(Void aVoid) {
        Log.d(TAG, "onPostExecute()");

        activity.sitelistParsed();
    }

}
