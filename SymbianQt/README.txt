Vasama is an application that shows lightning strikes on a map. It parses
spark.clx compatible (spark.txd + mapstrip) formatted data.

This app is currently targeted for Symbian^3 devices and I do my testing
on a Nokia N8. Requires Qt 4.7.

This is my first Qt+QML application ever, so the code probably looks quite
ugly.

Vasama is licensed under GPL v3. See LICENSE.txt.

For some kind of list of spark.txd locations, see
http://www.vaski.net/Foorumi/viewtopic.php?t=561 (in Finnish, sorry!)
or try googling for "spark.txd"
