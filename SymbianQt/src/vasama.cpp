/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

#include <QString>
#include <QDebug>
#include <QFile>
#include <QtNetwork>
#include <QDeclarativeProperty>
#include <QGraphicsObject>
#include <QObject>
#include <QDeclarativeEngine>
#include <QDeclarativeContext>
#include <QPixmapCache>

#include "vasama.h"
#include "gfxproviderstrikemap.h"
#include "gfxproviderstrikegraph.h"
#include "roleitemmodel.h"
#include "sitelist.h"
#include "version.h"


// =============================================================================
// Constaints
// =============================================================================

// Default data URL
// This weather station is maintained by Mika Ylikotila
const QString KDefaultBaseUrl = "http://nivalaweather.info/spark/";

// Remote data file name
const QString KSparkTxd = "spark.txd";

// Remote map strip file name
const QString KMapStrip = "mapstrip.png";

// Pi to a few digits
const float KPii = 3.14159f;

// Version string
const std::string KVersion = VASAMA_VERSION;

// User Agent String
const std::string KUserAgent = "Mozilla/5.0 (compatible; Vasama " + KVersion +
                               "; +http:/gitorious.org/vasama/)";

// Sitelist URL
const QUrl KSitelistUrl("http://vasama.pyksy.fi/spark/spark_sitelist.csv");

// Local methods

// =============================================================================
// Convert degrees to radians
// =============================================================================
//
double deg2rad(int degrees) {
    return (KPii * degrees) / 180;
}

// =============================================================================
// Convert x,y deltas to distance
// =============================================================================
//
double xy2distance(int dX, int dY) {
    return sqrt((double) dX*dX + dY*dY);
}

// Class methods

// =============================================================================
// Constructor
// =============================================================================
Vasama::Vasama(QDeclarativeView* view) :
    QObject(),
    m_view(NULL),
    m_strikeMapProvider(NULL),
    m_strikeRateGraphProvider(NULL),
    m_dlManager(NULL),
    m_dlReply(NULL),
    m_sitelistModel(NULL)
{
    qDebug() << Q_FUNC_INFO;

    m_view = view;

    addImageProviders();

    // Create sitelist model
    QHash<int, QByteArray> roleNames;
    roleNames[SitelistEntry::UrlRole] = "url";
    roleNames[SitelistEntry::LocationRole] = "location";
    roleNames[SitelistEntry::DateRole] = "date";
    m_sitelistModel = new RoleItemModel(roleNames);
    m_view->rootContext()->setContextProperty("vasamaSitelistModel",
                                              m_sitelistModel);

    // Load data Url from settings
    QSettings settings("Antti Kultanen", "Vasama");
    m_baseUrl = settings.value("BaseUrl").toString();

    readBaseUrls();

    // Register site history model
    m_view->rootContext()->setContextProperty(
            "vasamaSitesModel",
            QVariant::fromValue(m_baseUrlHistory));

    if (m_baseUrl.isEmpty()) {
        // Use default if not found
        m_baseUrl = KDefaultBaseUrl;
    }

    downloadSparkData();

    qDebug() << Q_FUNC_INFO << "m_baseUrl: " + m_baseUrl;
}


// =============================================================================
// Destructor
// =============================================================================
Vasama::~Vasama() {
    qDebug() << Q_FUNC_INFO;

    for (int i=0; i < m_strikeData.count(); i++) {
        delete m_strikeData.at(i);
    }

    delete m_sitelistModel;
    delete m_dlManager;
}

// =============================================================================
// downloadSparkData()
// =============================================================================
void Vasama::downloadSparkData() {
    qDebug() << Q_FUNC_INFO;

    emit showBusyIndicator(true);

    QNetworkRequest req(QUrl(m_baseUrl + KSparkTxd));
    req.setRawHeader("User-Agent", KUserAgent.c_str());
    m_dlReply = dlManager()->get(req);
    connect(m_dlReply,
             SIGNAL(finished()),
             this,
             SLOT(dataDownloadFinished()));
}

// =============================================================================
// dataDownloadFinished()
// =============================================================================
void Vasama::dataDownloadFinished() {
    qDebug() << Q_FUNC_INFO;

    int statusCode = m_dlReply->attribute(
                QNetworkRequest::HttpStatusCodeAttribute).toInt();
    qDebug() << Q_FUNC_INFO << "status: " << statusCode;

    if (m_dlReply->error() == QNetworkReply::NoError && statusCode == 200) {
        // Check if save needed
        saveBaseUrl();

        // Read bytes from the m_dlReply
        QByteArray bytes = m_dlReply->readAll();  // bytes
        QString string(bytes); // string
        string.remove(QChar('\r')); // strip MS-DOS carriage returns
        parseData(string);
    } else {
        emit httpError(statusCode);
    }

    emit showBusyIndicator(false);

    m_dlReply->deleteLater();
}

// =============================================================================
// parseData()
// =============================================================================
void Vasama::parseData(const QString& string) {
    qDebug() << Q_FUNC_INFO;

    m_headerLines.clear();
    QStringList lines = string.split("\n", QString::SkipEmptyParts);
    for (int i=0; i<8; i++) {
        QStringList words = lines[i].split("+", QString::SkipEmptyParts);
        m_headerLines.append(words[1]);
        qDebug() << Q_FUNC_INFO << "appending: " << words[1];
    }

    parseDateAndTime();
    parseLocation();
    parseStrikeRate();
    parseMapRange();
    parseStrikeData(lines);

    commitData();
    QPixmapCache::clear();
}

// =============================================================================
// parseDateAndTime()
// =============================================================================
void Vasama::parseDateAndTime() {
    qDebug() << Q_FUNC_INFO;

    /**
     * Date format in spark.txd:
     * DAT+0A0512*
     *     YYMMDD for year, month, day
     *
     * TIM+1637*
     *     SSSS for Seconds elapsed since midnight
     */

    bool ok;

    QString str = m_headerLines[0].right(7).left(2);
    int year = str.toInt(&ok, 16) + 2000;

    str = m_headerLines[0].right(5).left(2);
    int month = str.toInt(&ok, 16);

    str = m_headerLines[0].right(3).left(2);
    int day = str.toInt(&ok, 16);

    QDate date(year, month, day);

    str = m_headerLines[1].right(5).left(2);
    int hour = str.toInt(&ok, 16);

    str = m_headerLines[1].right(3).left(2);
    int minute = str.toInt(&ok, 16);

    QTime time(hour, minute);

    m_timeZone = m_headerLines[7];
    m_timeZone.remove(QChar('*'));

    m_dateTime.setDate(date);
    m_dateTime.setTime(time);
    // The times are from 2 hours past to present, hence -7200 seconds
    m_dateTime = m_dateTime.addSecs(-7200); 

    qDebug() << Q_FUNC_INFO << "timestamp: " <<
                m_dateTime.toString("dd.MM.yyyy hh:mm");
}

// =============================================================================
// parseLocation()
// =============================================================================
void Vasama::parseLocation() {
    qDebug() << Q_FUNC_INFO;

    /**
     * Location format in spark.txd:
     * LOC+Hameenlinna*
     *     LLLLLLLLLLL for location string in plain text
     */

    m_location = m_headerLines[6];
    m_location.remove(QChar('*'));
    qDebug() << Q_FUNC_INFO << "location: " << m_location;
}

// =============================================================================
// parseStrikeRate()
// =============================================================================
void Vasama::parseStrikeRate() {
    qDebug() << Q_FUNC_INFO;

    /**
     * Strike rate format in spark.txd:
     * SRT+003004005001004003002002001001001000001003003003002002000001 [ ... ]
     *     AAABBBCCCDDDEEE [ ... ] for strike rate in 12bit hexadecimal,
     *                             total 24 values
     */

    int maxStrikeRate = 0;
    m_strikeRate.clear();

    QString strikeString = m_headerLines[3];
    bool ok;

    for (int i=0; i<24; i++) {
        QString strike = strikeString.left(3*i + 3).right(3);
        int strikeRate = strike.toInt(&ok, 16);
        m_strikeRate.append(strikeRate);

        if (strikeRate > maxStrikeRate) {
            maxStrikeRate = strikeRate;
            m_strikeRateGraphProvider->setMaxStrikeRate(maxStrikeRate);
        }
    }
}

// =============================================================================
// parseMapRange()
// =============================================================================
void Vasama::parseMapRange() {
    qDebug() << Q_FUNC_INFO;

    /**
     * Map range format in spark.txd:
     * RNG+3E81F412C0*
           AAABBBCCC for 3 map ranges in 12bit hexadecimal
     */

    m_mapRange.clear();

    QString rangeString = m_headerLines[4];
    bool ok;

    QString rangePart = rangeString.left(3);
    int range = rangePart.toInt(&ok, 16);
    m_mapRange.append(range);
    qDebug() << Q_FUNC_INFO << "range: " << range;

    rangePart = rangeString.left(6).right(3);
    range = rangePart.toInt(&ok, 16);
    m_mapRange.append(range);
    qDebug() << Q_FUNC_INFO << "range: " << range;

    rangePart = rangeString.left(9).right(3);
    range = rangePart.toInt(&ok, 16);
    m_mapRange.append(range);
    qDebug() << Q_FUNC_INFO << " range: " << range;
}

// =============================================================================
// parseStrikeData()
// =============================================================================
void Vasama::parseStrikeData(QStringList& lines) {
    qDebug() << Q_FUNC_INFO;

    /**
     * Strike data format in spark.txd:
     * D05+23014419725223120A1982822330B3000000000000000000000000000000*
     *  TT BBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDDBBBDDD
     *  TT = Time elapsed in minutes in 8bit hexadecimal
     * BBB = Bearing in degrees (0-359) in 12bit decimal(!)
     * DDD = Distance in kilometres in 12bit hexadecimal
     */

    m_strikeData.clear();

    bool ok;

    for (int i=8; i<32; i++) {
        QStringList words = lines[i].split("+", QString::SkipEmptyParts);

        // First get the elapsed time
        StrikeData* data = new StrikeData;
        data->m_timeDelta = words[0].right(2).toInt(&ok, 16);

        // Parse the strike data
        for (int j=0; j<10; j++) {
            QString strike = words[1].left(j*6+6).right(6);
            int bearingDeg = strike.left(3).toInt(&ok, 10);
            int distance = strike.right(3).toInt(&ok, 16);

            // Break down the position to X and Y components
            double bearingRad = deg2rad(bearingDeg + 270);
            int x = distance * cos(bearingRad);
            int y = distance * sin(bearingRad);

            data->m_bearingDeg.append(bearingDeg);
            data->m_bearingRad.append(bearingRad);
            data->m_distance.append(distance);
            data->m_x.append(x);
            data->m_y.append(y);
        }

        m_strikeData.append(data);
    }
}

// =============================================================================
// commitData()
// =============================================================================
void Vasama::commitData() {
    qDebug() << Q_FUNC_INFO;

    QVariantMap dataMap;
    dataMap["location"] = m_location;
    dataMap["range1"] = QString(" %1 km").arg(m_mapRange[0]);
    dataMap["range2"] = QString(" %1 km").arg(m_mapRange[1]);
    dataMap["range3"] = QString(" %1 km").arg(m_mapRange[2]);
    dataMap["source"] = m_baseUrl + KMapStrip;
    dataMap["baseurl"] = m_baseUrl;
    emit dataParsed(dataMap);

    m_strikeMapProvider->setMapRange(m_mapRange);
    m_strikeMapProvider->setStrikeData(m_strikeData);

    m_strikeRateGraphProvider->setStrikeRateData(&m_strikeRate);
}

// =============================================================================
// addImageProviders()
// =============================================================================
void Vasama::addImageProviders() {
    qDebug() << Q_FUNC_INFO;

    m_strikeMapProvider = new GfxProviderStrikemap();
    m_view->engine()->addImageProvider(QLatin1String("vasama"),
                                       m_strikeMapProvider);

    m_strikeRateGraphProvider = new GfxProviderStrikeGraph();
    m_view->engine()->addImageProvider(QLatin1String("strikerate"),
                                       m_strikeRateGraphProvider);

    m_view->rootContext()->setContextProperty("vasamaData", this);
}

// =============================================================================
// getStrikeRate()
// =============================================================================
int Vasama::getStrikeRate(const int framenum) const {
    qDebug() << Q_FUNC_INFO;

    if (framenum < m_strikeRate.count()) {
        return m_strikeRate[ framenum ];
    } else {
        return 0;
    }
}

// =============================================================================
// getDateString()
// =============================================================================
QString Vasama::getDateString(const int framenum) const {
    qDebug() << Q_FUNC_INFO;

    QDateTime dateTime(m_dateTime.addSecs(60 * 5 * (framenum+1)));
    QString timestring = dateTime.toString("dd.MM.yy hh:mm");
    timestring.append(" ");
    timestring.append(m_timeZone);
    return timestring;
}

// =============================================================================
// baseUrl()
// =============================================================================
QString Vasama::baseUrl() const {
    return m_baseUrl;
}

// =============================================================================
// setBaseUrl()
// =============================================================================
void Vasama::setBaseUrl(const QString baseUrl) {
    qDebug() << Q_FUNC_INFO<< "Setting base url: " + baseUrl;

    m_baseUrl = baseUrl;

    if (m_baseUrl.left(7).toLower() != "http://") {
        m_baseUrl = "http://" + baseUrl;
    }
    if (m_baseUrl.right(9) == KSparkTxd) {
        m_baseUrl.remove(KSparkTxd);
    }
    if (m_baseUrl.right(1) != "/") {
        m_baseUrl.append("/");
    }

    downloadSparkData();
}

// =============================================================================
// getDistanceText()
// =============================================================================
QString Vasama::getDistanceText(int aX, int aY, int state) {
    qDebug() << Q_FUNC_INFO << "(" << aX << ", " << aY << ", " << state << ")";

    aX -= 120;
    aY -= 120;

    // calculate kilometres per pixel
    double kmratio = 0;
    if (state < m_mapRange.count()) {
        kmratio = m_mapRange[state] / 120.0;

        // multiply pixels to kilometres
        aX *= kmratio;
        aY *= kmratio;
    }

    // convert to distance
    double distance = xy2distance(aX, aY);
    qDebug() << Q_FUNC_INFO << "distance =" << distance;

    QString distanceText = QString::number(distance, 'f', 1) + " km";

    return distanceText;
}

// =============================================================================
// version()
// =============================================================================
QString Vasama::version() {
    qDebug() << Q_FUNC_INFO;

    return QString::fromStdString(KVersion);
}

// =============================================================================
// saveBaseUrl()
// =============================================================================
void Vasama::saveBaseUrl() {
    qDebug() << Q_FUNC_INFO;

    QSettings settings("Antti Kultanen", "Vasama");
    settings.setValue("BaseUrl", m_baseUrl);

    if (!m_baseUrlHistory.contains(m_baseUrl, Qt::CaseInsensitive)) {

        if (m_baseUrlHistory.count() > 7) {
            m_baseUrlHistory.removeAt(0);
        }

        m_baseUrlHistory.append(m_baseUrl);

        settings.beginWriteArray("BaseUrls");
        for (int i=0; i<m_baseUrlHistory.size(); i++) {
            settings.setArrayIndex(i);
            settings.setValue("BaseUrl", m_baseUrlHistory.at(i));
        }
        settings.endArray();
    }
}

// =============================================================================
// readBaseUrls()
// =============================================================================
void Vasama::readBaseUrls() {
    qDebug() << Q_FUNC_INFO;

    QSettings settings("Antti Kultanen", "Vasama");

    int arraySize = settings.beginReadArray("BaseUrls");
    for (int i=0; i < arraySize; i++) {
        settings.setArrayIndex(i);
        QString baseUrl = settings.value("BaseUrl").toString();
        m_baseUrlHistory.append(baseUrl);
    }

    settings.endArray();

    qDebug() << Q_FUNC_INFO << "count =" << m_baseUrlHistory.count();
}

// =============================================================================
// dlManager()
// =============================================================================
QNetworkAccessManager* Vasama::dlManager() {
    if (!m_dlManager) {
        m_dlManager = new QNetworkAccessManager(this);
    }
    return m_dlManager;
}

// =============================================================================
// downloadSitelist()
// =============================================================================
void Vasama::downloadSitelist() {
    qDebug() << Q_FUNC_INFO;

    if (m_sitelistModel->rowCount() == 0) {
        emit showBusyIndicator(true);

        qDebug() << Q_FUNC_INFO << "downloading site list";
        QNetworkRequest req(KSitelistUrl);
        req.setRawHeader("User-Agent", KUserAgent.c_str());

        m_dlReply = dlManager()->get(req);
        connect(m_dlReply,
                 SIGNAL(finished()),
                 this,
                 SLOT(sitelistDownloadFinished()));
    } else {
        emit showSitelist();
    }
}

// =============================================================================
// sitelistDownloadFinished()
// =============================================================================
void Vasama::sitelistDownloadFinished() {
    qDebug() << Q_FUNC_INFO;

    int statusCode = m_dlReply->attribute(
                QNetworkRequest::HttpStatusCodeAttribute).toInt();
    qDebug() << Q_FUNC_INFO << "status: " << statusCode;

    if (m_dlReply->error() == QNetworkReply::NoError && statusCode == 200) {
        // Read bytes from the m_dlReply
        QByteArray bytes = m_dlReply->readAll();  // bytes
        QString string(bytes); // string
        string.remove(QChar('\r')); // strip MS-DOS carriage returns

        parseSitelist(string);
    } else {
        emit httpError(statusCode);
    }

    emit showBusyIndicator(false);

    m_dlReply->deleteLater();
}

// =============================================================================
// parseSitelist()
// =============================================================================
void Vasama::parseSitelist(const QString& string) {
    qDebug() << Q_FUNC_INFO;

    if (m_sitelistModel) {
        m_sitelistModel->clear();
        QStringList lines = string.split("\n", QString::SkipEmptyParts);

        for (int i=0; i < lines.count(); i++) {
            QStringList cols = lines[i].split("\t", QString::KeepEmptyParts);
            QStandardItem* item = new QStandardItem();
            item->setData(cols[0], SitelistEntry::LocationRole);
            item->setData(cols[1].left(10), SitelistEntry::DateRole);
            item->setData(cols[2], SitelistEntry::UrlRole);
            m_sitelistModel->appendRow(item);
            qDebug() << cols[2];
        }
    }

    emit showSitelist();
}
