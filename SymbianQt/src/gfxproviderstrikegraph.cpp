/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

#include <QPainter>
#include <QDebug>

#include "gfxproviderstrikegraph.h"

// Colors of the lightning strike graph bars, tallest on top
const qint32 strikecolors[] =
{
    0xFFFFFF,
    0xFFFF00,
    0xFFCC00,
    0xFF9900,
    0xEE7700,
    0xAA6600,
    0xAA4400,
    0xAA2200,
    0xAA0000,
    0x990000,
    0x880000,
    0x660000
};

// Count of colors in array
const int KColorCount = 12;

// Image dimensions defined here for now
const int KWidth = 312;
const int KHeight = 64;

// =============================================================================
// Constructor
// =============================================================================
GfxProviderStrikeGraph::GfxProviderStrikeGraph() :
    m_strikeRate(NULL),
    m_maxStrikeRate(0)
{
    //
}

// =============================================================================
// Destructor
// =============================================================================
GfxProviderStrikeGraph::~GfxProviderStrikeGraph() {
    // Placeholder
}

// =============================================================================
// requestPixmap()
// =============================================================================
QPixmap GfxProviderStrikeGraph::requestPixmap(
        const QString &id,
        QSize *size,
        const QSize& /*requestedSize*/) {
    qDebug() << Q_FUNC_INFO << "framenum:" << id.toInt();

    if (m_maxStrikeRate && m_strikeRate && m_strikeRate->count() > 0) {
        bool ok;
        int framenum = id.toInt(&ok, 10);
        if(framenum < 0) framenum = 0;
        if(framenum > 23) framenum = 23;

        *size = QSize(KWidth, KHeight);

        // strikes per pixel
        double strikeRatio = (double) KHeight / m_maxStrikeRate;

        // pixels per color
        double colorRatio = (double) (KColorCount-1) / m_maxStrikeRate;

        // Create image, initially transparent with lines on top & bottom
        QImage* image = new QImage(*size, QImage::Format_ARGB32_Premultiplied);
        image->fill(QColor(0, 0, 0, 0).rgba());

        QPainter painter;
        painter.begin(image);
        painter.setPen(QColor(strikecolors[KColorCount-1]).rgba());

        // Loop from 0 to requested frame num
        for (int stepnum = 0; stepnum <= framenum; stepnum++) {
            int barHeight = m_strikeRate->at(stepnum) * strikeRatio;

            QRect rect(13 * stepnum+1, KHeight-barHeight, 10, barHeight);
            int colorindex = (m_maxStrikeRate-m_strikeRate->at(stepnum)) * colorRatio;
            unsigned int color = strikecolors[ colorindex ];
            QBrush brush(QColor(color).rgba());
            painter.setBrush(brush);
            painter.setPen(QColor(color).rgba());
            painter.drawRect(rect);
        }

        painter.end(); // finished

        delete m_pixmap;
        m_pixmap = new QPixmap;
        m_pixmap->convertFromImage(*image);
        delete image;
        image = NULL;

        return *m_pixmap;
    } else {
        // No strike data, return empty image.
        *size = QSize(1, 1);
        QPixmap map(*size);
        map.fill(Qt::transparent);
        qDebug() << Q_FUNC_INFO << "Return empty pixmap";
        return map;
    }
}

// =============================================================================
// setStrikeRateData()
// =============================================================================
void GfxProviderStrikeGraph::setStrikeRateData(QList<int>* strikeRate) {
    qDebug() << Q_FUNC_INFO;

    m_strikeRate = strikeRate;
}

// =============================================================================
// setMaxStrikeRate()
// =============================================================================
//
void GfxProviderStrikeGraph::setMaxStrikeRate(int& maxStrikeRate) {
    qDebug() << Q_FUNC_INFO << maxStrikeRate;

    m_maxStrikeRate = maxStrikeRate;
}
