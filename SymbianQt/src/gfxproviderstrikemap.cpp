/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

#include <QDebug>
#include <QPainter>

#include "gfxprovider.h"
#include "gfxproviderstrikemap.h"

// Colors of the lightning strikes, newest at top
const qint32 strikecolors[] =
{
    0x660000,
    0x880000,
    0x990000,
    0xAA0000,
    0xAA2200,
    0xAA4400,
    0xAA6600,
    0xEE7700,
    0xFF9900,
    0xFFCC00,
    0xFFFF00,
    0xFFFFFF
};

// Image dimensions defined here for now
const int KWidth = 240;
const int KHeight = 720;

// =============================================================================
// ctor
// =============================================================================
GfxProviderStrikemap::GfxProviderStrikemap()
{
    // Placeholder
}

// =============================================================================
// dtor
// =============================================================================
GfxProviderStrikemap::~GfxProviderStrikemap()
{
    // Placeholder
}

// =============================================================================
// requestPixmap()
// =============================================================================
QPixmap GfxProviderStrikemap::requestPixmap(
        const QString &id,
        QSize *size,
        const QSize& /*requestedSize*/) {
    qDebug() << Q_FUNC_INFO << "framenum:" << id.toInt();

    if (m_strikeData.count() > 0) {
        bool ok;
        int framenum = id.toInt(&ok, 10);
        if(framenum < 0) framenum = 0;
        if(framenum > 23) framenum = 23;

        *size = QSize(KWidth, KHeight);

        int startframe = 0;
        int colorindex = 0;

        if (framenum > 11) {
            // plotting 12 iterations from current-11 to current.
            startframe = framenum-11;
        } else {
            // plotting framenum iterations, starting with color 11-framenum
            colorindex = 11-framenum;
        }

        // Create image, initially transparent
        QImage* image = new QImage(*size, QImage::Format_ARGB32_Premultiplied);
        image->fill(QColor(0, 0, 0, 0).rgba());

        // Loop through strike data
        for (int frameidx = startframe; frameidx <= framenum && colorindex < 12; frameidx++) {
            StrikeData* strikeData = m_strikeData.at(frameidx);

            // loop through the 10 strikes for this strike set
            for (int strikenum=0; strikenum < 10; strikenum++) {
                int x = strikeData->m_x[ strikenum ];
                int y = strikeData->m_y[ strikenum ];

                if (x && y) {
                    // Plot the strike
                    QPainter painter;
                    painter.begin(image);
                    painter.setPen(QColor(strikecolors[ colorindex ]).rgba());
                    drawStrike(x, y, painter);
                    painter.end();
                }
            }

            // Increase color intensity for next round of strikes
            colorindex++;
        }

        delete m_pixmap;
        m_pixmap = new QPixmap;
        m_pixmap->convertFromImage(*image);
        delete image;
        image = NULL;

        return *m_pixmap;
    } else {
        // No strike data, return empty image.
        *size = QSize(1, 1);
        QPixmap map(*size);
        map.fill(Qt::transparent);
        qDebug() << Q_FUNC_INFO << "Return empty pixmap";
        return map;
    }
}

// =============================================================================
// drawStrike()
// =============================================================================
void GfxProviderStrikemap::drawStrike(const int& aX, const int& aY, QPainter& painter) {
    if (!aX && !aY)
    {
        return;
    }

    for (int i=0; i<3; i++)
    {
        // calculate kilometers per pixel
        double kmratio = m_mapRange.at(i) / 120.0;

        if (kmratio)
        {
            int x = aX / kmratio;
            int y = aY / kmratio;
            x += 120;
            y += 120;

            if (x >= 0 && x < 240 &&
                 y >= 0 && y < 240)
            {
                y += i * 240;
                painter.drawLine(x-3, y, x+3, y);
                painter.drawLine(x, y-3, x, y+3);
            }
        }
    }
}

// =============================================================================
// setStrikeData()
// =============================================================================
//
void GfxProviderStrikemap::setStrikeData(QList< StrikeData* >& strikeData) {
    qDebug() << Q_FUNC_INFO;

    m_strikeData = strikeData;
}

// =============================================================================
// setMapRange()
// =============================================================================
void GfxProviderStrikemap::setMapRange(QList< int >& mapRange)
{
    qDebug() << Q_FUNC_INFO;

    m_mapRange = mapRange;
}
