/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef STRIKEDATA_H
#define STRIKEDATA_H

#include <QVector>

class StrikeData
{
public:
    QList<int>    m_bearingDeg;
    QList<double> m_bearingRad;
    QList<int>    m_distance;
    QList<int>    m_x;
    QList<int>    m_y;

    int m_timeDelta;

};

#endif // STRIKEDATA_H
