/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GFXPROVIDERSTRIKEGRAPH_H
#define GFXPROVIDERSTRIKEGRAPH_H

#include "gfxprovider.h"

class GfxProviderStrikeGraph : public GfxProvider
{
public:
    GfxProviderStrikeGraph();

    ~GfxProviderStrikeGraph();

    QPixmap requestPixmap (const QString &id, QSize *size, const QSize &requestedSize);

    /**
     * Setter for strike rate data
     *
     * @param strikeRate strike rate data
     */
    void setStrikeRateData(QList<int>* strikeRate);

    /**
     * Setter for max strike rate
     *
     * @param maxStrikeRate max strike rate
     */
    void setMaxStrikeRate(int& maxStrikeRate);

private:

    // Array of 24 strike rates.
    QList<int>* m_strikeRate;

    // Maximum strike rate
    int         m_maxStrikeRate;

};

#endif // GFXPROVIDERSTRIKEGRAPH_H
