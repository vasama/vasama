/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef VASAMA_H
#define VASAMA_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QStringList>
#include <QDeclarativeView>
#include <QVariantMap>

#include "strikedata.h"
#include "gfxproviderstrikemap.h"
#include "gfxproviderstrikegraph.h"

// Forward declarations

class QDateTime;
class RoleItemModel;

// Class declaration

class Vasama : public QObject
{
    Q_OBJECT
public:
    explicit Vasama(QDeclarativeView* view);

    ~Vasama();

    /**
     * Initialize downloading the strike data.
     */
    Q_INVOKABLE void downloadSparkData();

    /**
     * Register the image providers to QML engine
     */
    void addImageProviders();

    /**
     * Get strike rate for frame number framenum. Called from QML
     *
     * @param framenum the frame number for which the strike rate
     *                 is being requested
     * @return int the strike rate
     */
    Q_INVOKABLE int getStrikeRate(const int framenum) const;

    /**
     * Get date string for frame number framenum. Called from QML
     *
     * @param framenum the frame number for which the date string
     *                 is being requested
     * @return QString the formatted date and time string
     */
    Q_INVOKABLE QString getDateString(const int framenum) const;

    /**
     * Get base url. Called from QML
     *
     * @return QString Base Url
     */
    Q_INVOKABLE QString baseUrl() const;

    /**
     * Set base url. Called from QML
     *
     * @param QString Base Url
     */
    Q_INVOKABLE void setBaseUrl(const QString baseUrl);

    /**
     * Get distance text. Called from QML
     *
     * @param QString Base Url
     */
    Q_INVOKABLE QString getDistanceText(int aX, int aY, int state);

    /**
     * Get version. Called from QML
     *
     * @return QString version string
     */
    Q_INVOKABLE QString version();

    /**
     * Download list of sites
     */
    Q_INVOKABLE void downloadSitelist();

signals:

    /**
     * Signaled to make sitelist visible
     */
    void showSitelist();

    /**
     * Signaled when data has been parsed
     *
     * @param map of parsed data
     */
    void dataParsed(QVariantMap sparkData);

    /**
     * Signaled when download resulted in an error
     *
     * @param int http response code
     */
    void httpError(int code);

    /**
     * Signaled to show/hide busy indicator during data download
     *
     * @param bool show busy indicator if true or hide
     */
    void showBusyIndicator (bool show);

private slots:

    /**
     * Called when the http download job finishes.
     */
    void dataDownloadFinished();

    /**
     * Called when the http download job finishes.
     */
    void sitelistDownloadFinished();

private:

    /**
     * Parse http download
     *
     * @string spark.txd downloaded via http
     */
    void parseData(const QString& string);

    /**
     * Parse strike data date and time
     */
    void parseDateAndTime();

    /**
     * Parse strike data location
     */
    void parseLocation();

    /**
     * Parse strike rates from data
     */
    void parseStrikeRate();

    /**
     * Parse map ranges from strike data
     */
    void parseMapRange();

    /**
     * Parse strike datas (24*10=240 strikes)
     *
     * @aparam lines String list of strike data lines
     */
    void parseStrikeData(QStringList& lines);

    /**
     * Update map ranges and location information to QML components
     */
    void commitData();

    /**
     * Save Url to settings if not already known
     */
    void saveBaseUrl();

    /**
     * Read Url history
     */
    void readBaseUrls();

    /**
     * Get DlManager
     *
     * @return Pointer to QNetworkAccessManager
     */
    QNetworkAccessManager* dlManager();

    /**
     * Parse http download
     *
     * @string sitelist downloaded via http
     */
    void parseSitelist(const QString& string);

private:

    // Data download URL
    QString             m_baseUrl;

    // Url history string list
    QStringList         m_baseUrlHistory;

    // List of raw header line strings
    QStringList         m_headerLines;

    // 24 arrays of parsed strike datas
    QList<StrikeData*>  m_strikeData;

    // Strike data timestamp
    QDateTime           m_dateTime;

    // Time zone string
    QString             m_timeZone;

    // Location string
    QString             m_location;

    // Array of 24 strike rates
    QList<int>          m_strikeRate;

    // Array of 3 map ranges
    QList<int>          m_mapRange;

    // View
    QDeclarativeView*   m_view;

    // Image providers for strike map and strike graph.
    // QDeclarativeEngine assumes ownership of the providers.
    GfxProviderStrikemap* m_strikeMapProvider;
    GfxProviderStrikeGraph* m_strikeRateGraphProvider;

    // Network access manager
    QNetworkAccessManager* m_dlManager;

    // Network reply
    QNetworkReply*      m_dlReply;

    // Sitelist model
    RoleItemModel*      m_sitelistModel;
};

#endif // VASAMA_H
