/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GFXPROVIDER_H
#define GFXPROVIDER_H

#include <QDeclarativeImageProvider>

class GfxProvider : public QDeclarativeImageProvider
{
public:
    GfxProvider () :
        QDeclarativeImageProvider(QDeclarativeImageProvider::Pixmap),
        m_pixmap(NULL)
    {
    }

    ~GfxProvider()
    {
        delete m_pixmap;
    }

    /**
     * Request pixmap for QML
     *
     * @param id contains frame number
     * @param size must be set to the image size
     * @param requestedSize contains requested image size
     * @return QPixmap returned to the QML
     */
    virtual QPixmap requestPixmap (const QString &id, QSize *size, const QSize &requestedSize) = 0;

protected:

    // The returned pixmap
    QPixmap* m_pixmap;

};

#endif // GFXPROVIDER_H
