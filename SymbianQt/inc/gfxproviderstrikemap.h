/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef GFXPROVIDERSTRIKEMAP_H
#define GFXPROVIDERSTRIKEMAP_H

#include "gfxprovider.h"
#include "strikedata.h"

class GfxProviderStrikemap : public GfxProvider
{
public:
    GfxProviderStrikemap();

    ~GfxProviderStrikemap();

    QPixmap requestPixmap (const QString &id, QSize *size, const QSize &requestedSize);

    /**
     * Setter for strike data
     *
     * @param strikeData strike data
     */
    void setStrikeData(QList< StrikeData* >& strikeData);

    /**
     * Setter for map ranges
     *
     * @param mapRange map ranges
     */
    void setMapRange(QList< int >& mapRange);

private:
    /**
     * Draw a lightning strike
     *
     * @param aX strike X coordinate
     * @param aY strike Y coordinate
     * @param painter Reference to painter instance
     */
    void drawStrike(const int& aX, const int& aY, QPainter& painter);

private:

    // Array of strike data
    QList<StrikeData*>  m_strikeData;

    // Array of 3 map ranges
    QList<int>          m_mapRange;
};

#endif // GFXPROVIDERSTRIKEMAP_H
