/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 1.1
import com.nokia.symbian 1.1

Item {
    id: aboutItem
    property bool show: false

    Rectangle {
        id: aboutWindow
        color: "black"
        opacity: 0.75
        anchors.fill: parent
        border.color: "grey"
        border.width: 2
        height: parent.height
        clip: true
    }

    MouseArea {
        anchors.fill: parent
        onPressed: privateStyle.play(Symbian.BasicButton)
        onClicked: aboutItem.show = false
    }

    Text {
        id: aboutText
        opacity: 1
        color: "white"
        anchors.fill: parent
        anchors.margins: 8
        height: parent.height
        wrapMode: Text.Wrap
        text: "Vasama " + vasamaData.version() + "<br>" +
              "<br>" +
              "Copyright 2011-2013 Antti Kultanen<br>" +
              "<a href=\"http://gitorious.org/vasama/\">http://gitorious.org/vasama/</a><br>" +
              "<br>" +
              "Vasama is licensed under GNU GPL v3.<br>" +
              "<a href=\"http://www.gnu.org/licenses/\">http://www.gnu.org/licenses/</a><br>" +
              "<br>" +
              "Background image by Tom de Vries.<br>" +
              "<a href=\"http://www.flickr.com/photos/tomdev/2706493904/\">http://www.flickr.com/photos/tomdev/2706493904/</a><br>" +
              "Image is licensed under CC BY-NC 2.0<br>" +
              "<a href=\"http://creativecommons.org/licenses/by-nc/2.0/\">http://creativecommons.org/licenses/by-nc/2.0/</a>"
        horizontalAlignment: Text.AlignLeft
        font.pixelSize: 14
        clip: true

        onLinkActivated: {
            Qt.openUrlExternally(link)
        }
    }
}
