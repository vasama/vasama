/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

Page {
    id: page
    orientationLock: PageOrientation.LockPortrait
    tools: vasamaTools

    function appForegroundChanged() {
        console.log("Vasama.qml appForegroundChanged() " + symbian.foreground)
        imageTimer.running = symbian.foreground
    }

    Component.onCompleted: {
        symbian.foregroundChanged.connect(appForegroundChanged)
    }
    Item  {
        id: keyHandler
        focus: true
        Keys.onPressed: {
            if (event.key === Qt.Key_Left || event.key === Qt.Key_Right) {
                if (event.key === Qt.Key_Left) {
                    strikemap.imagenumber--
                    if (strikemap.imagenumber < 0) strikemap.imagenumber = 23
                }
                if (event.key === Qt.Key_Right) {
                    strikemap.imagenumber++
                    if (strikemap.imagenumber > 23) strikemap.imagenumber = 0
                }

                imageTimer.restart();
                functionItem.updateImageData()
            }


            if (event.key === Qt.Key_Down) {
                if (image.y > -1*imageItem.height) image.y = -1*imageItem.height
                else if (image.y > -2*imageItem.height) image.y = -2*imageItem.height
            }

            if (event.key === Qt.Key_Up) {
                if (image.y < -1*imageItem.height) image.y = -1*imageItem.height
                else if (image.y < 0) image.y = 0
            }
        }
    }

    Connections {
        target: vasamaData
        onDataParsed: {
            image.source = sparkData["source"]
            sparkTitle.title = sparkData["location"]
            zoomrow.r1 = sparkData["range1"]
            zoomrow.r2 = sparkData["range2"]
            zoomrow.r3 = sparkData["range3"]
            urlTextInput.text = sparkData["baseurl"]
            strikemap.imagenumber = 0
        }
    }

    Image {
        source: "images/background.png"
        anchors.top: parent.top
        anchors.topMargin: -1 * privateStyle.statusBarHeight
        anchors.left: parent.left
        anchors.right: parent.right
        fillMode: Image.PreserveAspectCrop
    }

    Text {
        id: sparkTitle
        anchors.top: parent.top
        anchors.topMargin: 16
        width: parent.width
        font.pixelSize: 24
        horizontalAlignment: Text.AlignHCenter
        color: "white"
        property string title: "Vasama"
        text: title + "\n"
    }

    Rectangle {
        anchors.fill: imageItem
        anchors.margins: -1
        color: "transparent"
        border.width: 2
        border.color: "black"
        visible: !imageItem.doubleSize
    }

    Item {
        id: imageItem
        anchors.top: sparkTitle.bottom
        anchors.horizontalCenter: page.horizontalCenter
        width: doubleSize ? 480 : 240
        height: doubleSize ? 480 : 240
        clip: true
        property bool doubleSize: parent.width > 479
        property int strikemapY: 0

        MouseArea {
            id: imageMouseArea
            anchors.fill: parent
            drag.target: image
            drag.axis: Drag.YAxis
            drag.minimumY: -2 * imageItem.height
            drag.maximumY: 0
            property int oldY: 0

            onReleased: {
                privateStyle.play(Symbian.ItemDrop)
                var delta = (mouse.y > oldY) ? -0.15 : 0.15
                if (image.y < (-1.5+delta) * imageItem.height) {
                    image.y = -2 * imageItem.height
                    scalingText.text = zoomrow.r3
                }
                else if (image.y < (-0.5+delta) * imageItem.height) {
                    image.y = -1 * imageItem.height
                    scalingText.text = zoomrow.r2
                }
                else {
                    image.y = 0
                    scalingText.text = zoomrow.r1
                }
            }

            onPressed: {
                privateStyle.play(Symbian.ItemPick)
                oldY = mouse.y

                // Set distance text to bottom left of map
                urlTextInput.focus = false
                keyHandler.focus = true
                var distX = mouse.x
                var distY = mouse.y
                if (imageItem.doubleSize) {
                    distX /= 2
                    distY /= 2
                }

                distText.text = vasamaData.getDistanceText(distX, distY, -1 * image.y / imageItem.height)
                distText.visible = true
                distanceTimer.restart()
            }
        }

        Image {
            objectName: "image"
            id: image
            anchors.horizontalCenter: imageItem.horizontalCenter
            width: imageItem.doubleSize ? 480 : 240
            height: imageItem.doubleSize ? 1440 : 720
            y: 0
            property string mapStripPath: ""
            source: ""
            clip: true
            smooth: true
            fillMode: Image.PreserveAspectCrop
            Behavior on y { NumberAnimation { duration: 500; easing.type: Easing.InOutQuad } }
        }

        Image {
            id: strikemap
            anchors.horizontalCenter: imageItem.horizontalCenter
            width: imageItem.doubleSize ? 480 : 240
            height: imageItem.doubleSize ? 1440 : 720
            y: image.y
            property real imagenumber: 0
            source: "image://vasama"
            clip: true
            smooth: false
            fillMode: Image.PreserveAspectCrop
        }

        Image {
            id: crosshair
            source: "images/crosshair.png"
            width: 7
            height: 7
            anchors.horizontalCenter: imageItem.horizontalCenter
            anchors.verticalCenter: imageItem.verticalCenter
        }

        Timer {
            id: imageTimer
            interval: 1000
            repeat: true
            running: true
            onTriggered: {
                functionItem.updateImageData()

                strikemap.imagenumber++
                strikemap.imagenumber %= 24
            }
        }

        Timer {
            id: distanceTimer
            interval: 3000
            repeat: false
            running: true
            onTriggered: {
                distText.visible = false
                distText.text = ""
            }
        }
    }

    Rectangle {
        id: topBlackRect
        color: "black"
        anchors.top: imageItem.top
        anchors.left: imageItem.left
        anchors.right: imageItem.right
        height: counterText.height
        opacity: 0.25
    }

    Rectangle {
        color: "black"
        anchors.left: imageItem.left
        anchors.right: imageItem.right
        anchors.top: topBlackRect.bottom
        height: distText.height
        opacity: 0.25
        visible: distText.visible
    }

    Rectangle {
        color: "black"
        anchors.left: imageItem.left
        anchors.right: imageItem.right
        anchors.bottom: imageItem.bottom
        height: timeText.height
        opacity: 0.25
    }

    Label {
        id: counterText
        width: (imageItem.width / 2)
        text: "Rate " + strikerate.toString() + "/min"
        anchors.top: imageItem.top
        anchors.left: imageItem.left
        anchors.leftMargin: 8
        property real strikerate: 0
    }

    Label {
        id: scalingText
        width: (imageItem.width / 2)
        text: zoomrow.r1
        anchors.top: imageItem.top
        anchors.right: imageItem.right
        anchors.rightMargin: 8
        horizontalAlignment: Text.AlignRight
    }

    Label {
        objectName: "distText"
        id: distText
        width: (imageItem.width / 2)
        text: ""
        anchors.top: scalingText.bottom
        anchors.right: imageItem.right
        anchors.rightMargin: 8
        horizontalAlignment: Text.AlignRight
        visible: false
    }

    Label {
        objectName: "timeText"
        id: timeText
        property string timetextcontent
        width: (imageItem.width / 2)
        text: timetextcontent
        anchors.bottom: imageItem.bottom
        anchors.right: imageItem.right
        anchors.rightMargin: 8
        horizontalAlignment: Text.AlignRight
    }

    Rectangle {
        anchors.fill: strikeRateGraph
        color: "black"
        opacity: 0.5
    }

    Rectangle {
        id: graphFrameRect
        anchors.fill: strikeRateGraph
        anchors.margins: -2
        color: "transparent"
        border.width: 2
        border.color: "black"
    }

    Image {
        id: strikeRateGraph
        source: "image://strikerate"
        width: 312
        height: 64
        anchors.horizontalCenter: page.horizontalCenter
        anchors.top: timeText.bottom
        anchors.topMargin: 16
        property int graphnum: 0

        onGraphnumChanged: {
            if (strikeGraphMouseArea.pressed) {
                privateStyle.play(Symbian.ItemScroll)
            }
        }

        MouseArea {
            id: strikeGraphMouseArea
            anchors.fill: parent

            onPressed: {
                imageTimer.stop();
            }

            onReleased: {
                imageTimer.start();
            }

            onPositionChanged: {
                strikeRateGraph.graphnum = (mouse.x / (strikeRateGraph.width / 24)).toFixed(0)
                if (strikeRateGraph.graphnum < 0) strikeRateGraph.graphnum = 0
                if (strikeRateGraph.graphnum > 23) strikeRateGraph.graphnum = 23
                strikemap.imagenumber = strikeRateGraph.graphnum
                functionItem.updateImageData()
            }
        }
    }

    // Buttons row
    Row {
        objectName: "zoomrow"
        id: zoomrow
        spacing: 4
        property real w: width/3
        anchors.top: graphFrameRect.bottom
        anchors.topMargin: 16
        anchors.left: graphFrameRect.left
        anchors.right: graphFrameRect.right
        anchors.rightMargin: 4
        property string r1: "XXX km"
        property string r2: "YYY km"
        property string r3: "ZZZ km"

        Button {
            width: zoomrow.w
            font.pixelSize: 20
            text: zoomrow.r1
            onClicked: {
                image.y = 0
                scalingText.text = zoomrow.r1
            }
        }

        Button {
            width: zoomrow.w
            font.pixelSize: 20
            text: zoomrow.r2
            onClicked: {
                image.y = -1*imageItem.height
                scalingText.text = zoomrow.r2
            }
        }

        Button
        {
            width: zoomrow.w
            font.pixelSize: 20
            text: zoomrow.r3
            onClicked: {
                image.y = -2*imageItem.height
                scalingText.text = zoomrow.r3
            }
        }
    }

    Item {
        id: functionItem
        function updateImageData() {
            strikemap.source = "image://vasama/" + strikemap.imagenumber.toString()
            strikeRateGraph.source = "image://strikerate/" + strikemap.imagenumber.toString()

            counterText.strikerate = vasamaData.getStrikeRate(strikemap.imagenumber)
            timeText.timetextcontent = vasamaData.getDateString(strikemap.imagenumber)
        }
    }

    Rectangle {
        anchors.top: urlTextInput.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        color: "black"
        opacity: inputContext.visible ? 0.75 : 0
        visible: inputContext.visible
    }

    TextField {
        id: urlTextInput
        anchors.topMargin: 16
        anchors.left: parent.left
        anchors.leftMargin: 16
        width: page.width - 2*anchors.leftMargin - comboItem.width
        anchors.rightMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 32
        height: 40

        function submitUrl() {
            urlTextInput.closeSoftwareInputPanel()
            urlTextInput.focus = false
            vasamaData.setBaseUrl(urlTextInput.text)
        }

        Keys.onReturnPressed: {
            submitUrl()
        }
        Keys.onEnterPressed: {
            submitUrl()
        }
    }

    Button {
        id: comboItem
        anchors.top: urlTextInput.top
        anchors.left: urlTextInput.right
        iconSource: privateStyle.imagePath("qtg_graf_choice_list_indicator", false)
        onClicked: siteHistory.show = !siteHistory.show
    }

    MouseArea {
        anchors.fill: parent
        enabled: siteHistory.visible

        onPressed: privateStyle.play(Symbian.BasicButton)
        onClicked: siteHistory.show = false
    }

    Sitehistory {
        id: siteHistory
        objectName: "siteHistory"
        width: page.width - 32
        height: show ? listheight : 0
        visible: height > 0
        anchors.horizontalCenter: imageItem.horizontalCenter
        anchors.bottom: urlTextInput.top
        property bool show: false
        Behavior on height { NumberAnimation { duration: 250; easing.type: Easing.InOutQuad } }
    }

    MouseArea {
        anchors.fill: parent
        enabled: about.show
        onPressed: privateStyle.play(Symbian.BasicButton)
        onClicked: about.show = false
    }

    About {
        id: about
        anchors.horizontalCenter: imageItem.horizontalCenter
        anchors.verticalCenter: imageItem.verticalCenter
        width: page.width - 4
        height: show ? 232 : 0
        visible: height > 0
        show: false
        Behavior on height { NumberAnimation { duration: 420; easing.type: Easing.InOutQuad } }
    }

    MouseArea {
        anchors.fill: parent
        enabled: sitelist.visible

        onPressed: privateStyle.play(Symbian.BasicButton)
        onClicked: sitelist.height = 0
    }

    Sitelist {
        id: sitelist
        objectName: "sitelist"

        height: 0
        anchors.bottom: urlTextInput.bottom
        width: parent.width - 32
        anchors.horizontalCenter: parent.horizontalCenter
        visible: sitelist.height > 0
        clip: true
        Behavior on height { NumberAnimation { duration: 250; easing.type: Easing.InOutQuad } }

        Connections {
            target: vasamaData
            onShowSitelist: {
                sitelist.height = vasamaSitelistModel.count > 10 ? 480 : 48*vasamaSitelistModel.count
            }
        }
    }

    BusyIndicator {
        id: busy
        running: visible
        width: 60
        height: 60
        anchors.centerIn: imageItem

        Connections {
            target: vasamaData
            onShowBusyIndicator: {
                busy.visible = show
            }
        }
    }

    InfoBanner {
        id: banner

        Connections {
            target: vasamaData
            onHttpError: {
                banner.text = "HTTP Error: " + code
                banner.timeout = 5000
                banner.open()
            }
        }
    }

    Connections {
        target: vasamaTools
        onBackClicked: {
            if (sitelist.height > 0) {
                sitelist.height = 0
            } else {
                Qt.quit()
            }
        }
        onDirectoryClicked: {
            if (sitelist.height > 0) {
                sitelist.height = 0
            } else {
                vasamaData.downloadSitelist()
            }
        }

        onMenuClicked: {
            optionsMenu.open()
        }
    }

    Toolz {
        id: vasamaTools
    }

    Connections {
        target: optionsMenu

        onReloadClicked: {
            vasamaData.setBaseUrl(urlTextInput.text)
        }
        onDirectoryClicked: {
            vasamaData.downloadSitelist()
        }
        onAboutClicked: {
            about.show = !about.show
        }
    }

    OptionsMenu {
        id: optionsMenu
    }
}
