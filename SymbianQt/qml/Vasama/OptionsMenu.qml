// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

Menu {
    signal directoryClicked()
    signal reloadClicked()
    signal aboutClicked()

    content: MenuLayout {
        MenuItem {
            text: "Directory"
            onClicked: directoryClicked()
        }
        MenuItem {
            text: "Reload"
            onClicked: reloadClicked()
        }
        MenuItem {
            text: "About"
            onClicked: aboutClicked()
        }
        MenuItem {
            text: "Exit"
            onClicked: Qt.quit()
        }
    }
}
