/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

PageStackWindow {
    id: window
    initialPage: Vasama {}
    showStatusBar: true
    showToolBar: true
    platformSoftwareInputPanelEnabled: true
}
