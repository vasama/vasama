// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

ToolBarLayout {
    id: toolsVasama

    signal backClicked()
    signal directoryClicked()
    signal menuClicked()

    ToolButton {
        iconSource: privateStyle.imagePath("toolbar-back", false)
        onClicked: backClicked()
    }

    ToolButton {
        iconSource: privateStyle.imagePath("toolbar-search", false)
        onClicked: directoryClicked()
    }

    ToolButton {
        iconSource: privateStyle.imagePath("toolbar-menu", false)
        onClicked: menuClicked()
    }
}
