/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 1.1
import com.nokia.symbian 1.1

Item {
    id: sitelist
    clip: true

    BorderImage {
        id: borderImage
        source: "images/lineedit.png"
        anchors.fill: parent
        border.left: 10
        border.top: 10
        border.bottom: 10
        border.right: 10
    }

    ListView {
        id: listView
        anchors.fill: borderImage
        model: vasamaSitelistModel
        delegate: sitelistDelegate
    }

    ScrollBar {
        flickableItem: listView
        anchors.right: listView.right
    }

    Component {
        id: sitelistDelegate

        Rectangle {
            id: item

            color: "#00000000"
            width: sitelist.width
            height: 48
            z: 10

            Text {
                id: locationText
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 20
                anchors.rightMargin: 20
                anchors.topMargin: 5

                text: location
                font.pixelSize: 18
                font.bold: true
                color: "black"
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignTop
            }

            Text {
                id: urlText
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 20
                anchors.rightMargin: 20
                anchors.bottomMargin: 5

                text: url
                font.pixelSize: 14
                font.bold: false
                color: "black"
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignBottom
            }

            Text {
                id: dateText
                anchors.top: parent.top
                anchors.topMargin: 5
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 20
                anchors.rightMargin: 20
                // anchors.bottom: parent.bottom
                // anchors.bottomMargin: 5

                text: date
                font.pixelSize: 14
                font.bold: false
                color: "black"
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignBottom
            }

            Rectangle {
                color: "black"
                visible: index > 0
                z: 20
                width: sitelist.width
                anchors.top: item.top
                height: 1
            }

            MouseArea {
                anchors.fill: parent
                onPressed: {
                    privateStyle.play(Symbian.BasicButton)
                    locationText.color = "white"
                    urlText.color = "white"
                    item.color = "black"
                }

                onReleased: {
                    locationText.color = "black"
                    urlText.color = "black"
                    item.color = "#00000000"

                }

                onCanceled: {
                    locationText.color = "black"
                    urlText.color = "black"
                    item.color = "#00000000"
                }

                onClicked: {
                    vasamaData.setBaseUrl(url)
                    sitelist.height = 0
                }
            }
        }
    }
}
