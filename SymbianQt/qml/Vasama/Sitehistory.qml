/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

import QtQuick 1.1
import com.nokia.symbian 1.1

Item {
    id: history
    property int listheight: vasamaSitesModel.length * itemheight
    property int itemheight: 40
    height: 0

    BorderImage {
        source: "images/lineedit.png"
        anchors.fill: parent
        border.left: 10
        border.top: 10
        border.bottom: 10
        border.right: 10
    }

    Component {
        id: listDelegate

        Item {
            id: delegateItem
            width: page.width - 32
            height: history.itemheight

            BorderImage {
                id: siteBorder

                source: "images/button-grey.png"
                clip: true
                anchors.fill: parent
                visible: false

                border
                {
                    left: 10
                    top: 10
                    right: 10
                    bottom: 10
                }
            }

            Text {
                id: delegateText
                text: modelData
                font.pixelSize: 16
                // font.family: theme.fontFamilyHeading
                font.bold: true
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 10
            }

            MouseArea {
                anchors.fill: parent
                onPressed: {
                    privateStyle.play(Symbian.BasicButton)
                    delegateText.color = "white"
                    siteBorder.visible = true
                }

                onReleased: {
                    delegateText.color = "black"
                    siteBorder.visible = false
                }

                onCanceled: {
                    delegateText.color = "black"
                    siteBorder.visible = false
                }

                onClicked: {
                    vasamaData.setBaseUrl(modelData)
                    siteHistory.show = false
                }
            }
        }
    }

    // SilicaListView {
    ListView {
        id: listView
        anchors.fill: parent
        model: vasamaSitesModel
        delegate:  listDelegate
        clip: true
    }
}
