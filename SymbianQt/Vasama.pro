# Add files and directories to ship with the application 
# by adapting the examples below.
# file1.source = myfile
# dir1.source = mydir
DEPLOYMENTFOLDERS = # file1 dir1

symbian {
    TARGET.UID3 = 0xE5A83F00
    LIBS += -lcone -leikcore -lavkon
}
# Smart Installer package's UID
# This UID is from the protected range 
# and therefore the package will fail to install if self-signed
# By default qmake uses the unprotected range value if unprotected UID is defined for the application
# and 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += LocalServices NetworkServices ReadUserData UserEnvironment WriteUserData

# If your application uses the Qt Mobility libraries, uncomment
# the following lines and add the respective components to the 
# MOBILITY variable. 
CONFIG += mobility
# MOBILITY +=

QT += declarative
QT += network
# QT += opengl

CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

INCLUDEPATH += inc

SOURCES += main.cpp \
    src/vasama.cpp \
    src/gfxproviderstrikemap.cpp \
    src/gfxproviderstrikegraph.cpp \
    src/roleitemmodel.cpp
HEADERS += \
    inc/strikedata.h \
    inc/gfxprovider.h \
    inc/vasama.h \
    inc/gfxproviderstrikemap.h \
    inc/gfxproviderstrikegraph.h \
    inc/version.h \
    inc/roleitemmodel.h \
    inc/sitelist.h
FORMS +=

# Please do not modify the following two lines. Required for deployment.
include(deployment.pri)
qtcAddDeployment()

OTHER_FILES += \
    qml/Vasama/Vasama.qml \
    qml/Vasama/Sitelist.qml \
    qml/Vasama/Sitehistory.qml \
    qml/Vasama/About.qml \
    qml/Vasama/main.qml \
    qml/Vasama/Toolz.qml \
    qml/Vasama/OptionsMenu.qml
