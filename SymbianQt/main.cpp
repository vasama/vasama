/*
    Copyright 2011-2013 Antti Kultanen <antti.kultanen@molukki.com>

    This file is part of Vasama.

    Vasama is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Vasama is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Vasama. If not, see <http://www.gnu.org/licenses/>.

*/

#include "vasama.h"

#include <QtGui/QApplication>
#include <QDeclarativeView>
#include <QtGui>

#ifdef Q_OS_SYMBIAN
#include <eikenv.h>
#include <eikappui.h>
#include <aknenv.h>
#include <aknappui.h>
#endif


int main(int argc, char *argv[])
{
#ifdef Q_OS_SYMBIAN
    QApplication::setGraphicsSystem(QLatin1String("openvg"));
#endif // Q_OS_SYMBIAN

    QApplication app(argc, argv);
    QScopedPointer<QDeclarativeView> view(new QDeclarativeView);

    // Show splash
    QScopedPointer<QPixmap> pixmap(new QPixmap("qml/Vasama/images/splash.png"));
    QScopedPointer<QSplashScreen> splash(new QSplashScreen(*pixmap.data()));
    if (!splash.isNull()) {
        splash->showFullScreen();
        splash->raise();
    }

    QScopedPointer<Vasama> vasama(new Vasama(view.data()));

    // Connect QML engine quit() to QApplication::quit()
    QObject::connect((QObject*) view->engine(),
                     SIGNAL(quit()),
                     &app,
                     SLOT(quit()));
    view->setSource(QUrl::fromLocalFile("qml/Vasama/main.qml"));

#ifdef Q_OS_SYMBIAN
    CAknAppUi* appUi = dynamic_cast<CAknAppUi*> (CEikonEnv::Static()->AppUi());
    if (appUi)
    {
        // Lock application orientation into landscape
        appUi->SetOrientationL(CAknAppUi::EAppUiOrientationPortrait);
    }

    view->showFullScreen();
#else
    view->show();
#endif // Q_OS_SYMBIAN

    if (!splash.isNull()) {
        splash->finish(view.data());
    }

    return app.exec();
}
