- Landscape UI (currently locked to portrait) (probably not worth the effort)

- Homescreen widget (fat chance: the Belle style widgets API was never released to the public)

- Meego Harmageddon port

- Sailfish OS port

- Fix deployment stuff in .pro to automatically create valid installer.pkg

- Make image flickable instead of draggable

- Get a better appicon
